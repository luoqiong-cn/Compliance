﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Word = Microsoft.Office.Interop.Word;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;

namespace FundManager
{
    class Program
    {
        static void Main(string[] args)
        {
            FundManagerChange();
        }


        /// <summary>
        /// 基金经理简历
        /// </summary>
        static void FundManagerResume()
        {
            File.Delete(Directory.GetCurrentDirectory() + "\\基金经理.docx");

            object oMissing = System.Reflection.Missing.Value;

            Word.Application app = new Word.Application();
            Word.Document doc = app.Documents.Open(Directory.GetCurrentDirectory() + "\\模板.docx");//app.Documents.Add();
            Word.Range tableLocation = app.ActiveDocument.Range(0, 0);
            Word.Table table = doc.Tables[1];//doc.Tables.Add(tableLocation, 1, 7);

            Excel.Application ExcelApp = new Excel.Application();
            Excel.Workbook sourceWbk = ExcelApp.Workbooks.Open(Directory.GetCurrentDirectory() + "\\基金经理信息——标准模板.xlsx");
            Excel.Worksheet sourceSheet = sourceWbk.Sheets[1];
            Excel.Range sourceRange = sourceSheet.UsedRange;

            string previousFM = "", currentFM = "";
            string previousJL = "";
            int start = 2, end = 2; //记录起止位置，合并用
            int idx_FM = 0; //基金经理的序号
            int wordRowCount = 0;

            //这一个循环把所有数据复制到word里
            for (int i = 2; i <= sourceRange.Rows.Count; i++)
            {
                table.Rows.Add();
                wordRowCount = table.Rows.Count;

                table.Cell(wordRowCount, 1).Range.Text = "1";   //序号
                table.Cell(wordRowCount, 2).Range.Text = sourceRange.Cells[i, 2].Value; //FM
                table.Cell(wordRowCount, 3).Range.Text = sourceRange.Cells[i, 1].Value; //基金名称
                DateTime dt = sourceRange.Cells[i, 4].Value;
                table.Cell(wordRowCount, 4).Range.Text = dt.ToString("yyyy-MM-dd");  //任期
                table.Cell(wordRowCount, 5).Range.Text = "";    //学历
                table.Cell(wordRowCount, 6).Range.Text = sourceRange.Cells[i, 9].Value; //经历
            }
            table.Rows.Add();
            previousFM = table.Cell(2, 2).Range.Text;

            Console.WriteLine(string.Format("共{0}行", table.Rows.Count));
            for (int i = 3; i <= table.Rows.Count; i++)
            {
                Console.WriteLine(i);
                end++;
                currentFM = table.Cell(i, 2).Range.Text;

                if (!currentFM.Equals(previousFM))
                {
                    if (end - 1 > start)    //只有多于一行才需要合并单元格
                    {
                        table.Cell(start, 1).Merge(table.Cell(end - 1, 1));

                        table.Cell(start, 2).Merge(table.Cell(end - 1, 2));
                        table.Cell(start, 2).Range.Text = previousFM;

                        table.Cell(start, 5).Merge(table.Cell(end - 1, 5));

                        previousJL = table.Cell(start, 6).Range.Text;
                        table.Cell(start, 6).Merge(table.Cell(end - 1, 6));
                        table.Cell(start, 6).Range.Text = previousJL;

                        table.Cell(start, 7).Merge(table.Cell(end - 1, 7));
                    }

                    table.Cell(start, 1).Range.Text = idx_FM.ToString();
                    idx_FM++;
                    start = end;
                    previousFM = currentFM;
                }
            }
            //table.Rows[2].Delete();

            table.Borders.InsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;
            table.Borders.OutsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;

            doc.SaveAs2(Directory.GetCurrentDirectory() + "\\基金经理.docx");
            doc.Close(false);
            sourceWbk.Close(false);
        }

        /// <summary>
        /// 基金经理变更
        /// </summary>
        static void FundManagerChange()
        {
            File.Delete(Directory.GetCurrentDirectory() + "\\基金经理变更.docx");

            object oMissing = System.Reflection.Missing.Value;

            Word.Application app = new Word.Application();
            Word.Document destDoc = app.Documents.Open(Directory.GetCurrentDirectory() + "\\基金经理变更模板.docx");
            Word.Table destTable = destDoc.Tables[1];
            int idx = 0;
            char[] esp = new char[]{'\r','\n','\a'};

            foreach (string filename in Directory.GetFiles(@"\\hfm-pubshare\HFM各部门共享\合规部\临时公告存档\2017年公告", "*基金经理*.doc*", SearchOption.AllDirectories))
            {
                try
                {
                    idx++;
                    Word.Document doc = app.Documents.Open(filename, oMissing, true);
                    //Console.WriteLine("正在处理：" + filename);

                    //遍历doc里的tables
                    //第一个table是基本信息，第二个（如果有）是新任基金经理信息，第三个（如果有）是离任基金经理信息, 如果一个table都没有，就略过
                    if (doc.Tables.Count > 0)
                    {
                        destTable.Rows.Add();
                        int rowCount = destTable.Rows.Count;
                        string temp = "";

                        //处理table[1]
                        destTable.Cell(rowCount, 1).Range.Text = idx.ToString();    //序号
                        destTable.Cell(rowCount, 2).Range.Text = doc.Tables[1].Cell(2, 2).Range.Text.Trim(esp);  //基金简称


                        for (int i = 1; i <= doc.Tables[1].Rows.Count; i++)
                        {
                            if (doc.Tables[1].Cell(i, 1).Range.Text.Substring(0, 2).Equals("新任"))
                            {
                                destTable.Cell(rowCount, 5).Range.Text = doc.Tables[1].Cell(i, 2).Range.Text.Trim(esp);
                                destTable.Cell(rowCount, 4).Range.Text = doc.Tables[2].Cell(2, 2).Range.Text.Trim(esp);   //离任日期
                            }

                            if (doc.Tables[1].Cell(i, 1).Range.Text.Substring(0, 2).Equals("离任"))
                            {
                            }

                            if (doc.Tables[1].Cell(i, 1).Range.Text.Substring(0, 2).Equals("共同"))
                            {
                                destTable.Cell(rowCount, 6).Range.Text = string.Format("与原基金经理{0}共同管理", doc.Tables[1].Cell(i, 2).Range.Text.Trim(esp));
                                temp = doc.Tables[1].Cell(i, 2).Range.Text.Trim(esp);
                            }
                        }

                        if (temp.Length > 4)
                        {
                            temp = string.Format("；由原基金经理{0}共同管理", temp);
                        }
                        else if (temp.Length > 0)
                        {
                            temp = string.Format("；由原基金经理{0}单独管理", temp);
                        }

                        //离任
                        if (doc.Tables[2].Cell(1, 1).Range.Text.Substring(0, 2).Equals("离任"))
                        {
                            destTable.Cell(rowCount, 3).Range.Text = doc.Tables[2].Cell(1, 2).Range.Text.Trim(esp);   //姓名
                            destTable.Cell(rowCount, 4).Range.Text = doc.Tables[2].Cell(3, 2).Range.Text.Trim(esp);   //离任日期
                            destTable.Cell(rowCount, 6).Range.Text = doc.Tables[2].Cell(2, 2).Range.Text.Trim(esp) + temp;   //备注
                        }
                        else if (doc.Tables.Count == 3)
                        {
                            destTable.Cell(rowCount, 3).Range.Text = doc.Tables[3].Cell(1, 2).Range.Text.Trim(esp);   //姓名
                            destTable.Cell(rowCount, 4).Range.Text = doc.Tables[3].Cell(3, 2).Range.Text.Trim(esp);   //离任日期
                            destTable.Cell(rowCount, 6).Range.Text = doc.Tables[3].Cell(2, 2).Range.Text.Trim(esp) + temp;   //备注

                        }

                        
                    }

                    doc.Close(false);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(filename + ex.Message);
                }
            }

            destDoc.SaveAs2(Directory.GetCurrentDirectory() + "\\基金经理变更.docx");
            destDoc.Close(false);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;

using Excel = Microsoft.Office.Interop.Excel;
using cn.jsfund.compliance.Utils;
using System.Text.RegularExpressions;
using Microsoft.Office.Interop.Excel;

namespace ConsoleAppForTesting
{
    class Program
    {
        static void Main(string[] args)
        {
            string outputPath;
            if (args.Length > 0)
                outputPath = args[0] + "\\";
            else
                outputPath = DataSources.OUTPUT_PATH;

            string strDate = System.DateTime.Now.ToString("yyyyMMdd");

            //准备输出目录
            if (Directory.Exists(outputPath + strDate))
                Directory.Delete(outputPath + strDate, true);
            Directory.CreateDirectory(outputPath + strDate);            

            string PingAnPath = String.Format("{0}{1}\\平安委托帐户股票禁投{1}.XLS", DataSources.PINGAN_FBD, strDate);
            string PingAnLmtPath = String.Format("{0}{1}\\平安委托帐户股票限投{1}.XLS", DataSources.PINGAN_FBD, strDate);
            string GlfPath = String.Format("{0}{1}\\太平洋人寿-关联方禁投池{1}.XLS", DataSources.GLF_FBD, strDate);
            string TpylPath = String.Format("{0}{1}\\禁投证券池(太平养老){1}.XLS", DataSources.TPYL_FBD, strDate);
            //string RSPath = String.Format("{0}{1}\\标准版{1}.XLS", DataSources.RS_FBD, strDate);
            string RSPath = String.Format("{0}{1}\\标准版{1}.XLS", DataSources.RS_FBD, strDate);
            string TKPath_EQ = String.Format("{0}{1}\\泰康养老投资年金黑名单_股票_{1}.XLS", DataSources.TK_FBD, strDate);
            string TKPath_FI = String.Format("{0}{1}\\泰康养老投资年金黑名单_债券_{1}.XLSX", DataSources.TK_FBD, strDate);
            string TKPath_OF = String.Format("{0}{1}\\泰康养老投资年金黑名单_基金_{1}.XLS", DataSources.TK_FBD, strDate);

            #region 长江受托年金禁投池
            /*
            //长江受托组合，使用文件：\\hfm-ywshare\ZXData\CaiHui\长江养老禁投。禁止池=禁止股票池+禁止债券池，限制池用限制基金池，全名单替换。
            //涉及组合：长江金色、上海烟草、中国建材
            string cjPath = string.Format(@"\\hfm-ywshare\ZXData\CaiHui\长江养老禁投\{0}\CJYLJTC_JYNEW{0}.xls", strDate);
            //之前的processor不能源和config一个文件，先复制一份
            string cjConfigPath = Directory.GetCurrentDirectory() + "/config.xls";
            File.Copy(cjPath, cjConfigPath, true);
            PoolProcessor cj = new PoolProcessor(cjPath, "", cjConfigPath, "禁投债券池", DataSources.getOutputFileName(outputPath, "长江受托年金禁止池"));
            cj.process();
            File.Delete(cjConfigPath);
            //长江组合各自追加股票
            foreach (string fundName in "上海烟草,长江金色,中国建材".Split(','))
            {
                cj = new PoolProcessor(DataSources.getOutputFileName(outputPath, "长江受托年金禁止池"),
                    "",
                    DataSources.CONFIG,
                    fundName,
                    DataSources.getOutputFileName(outputPath, "长江受托年金禁止池_"+fundName));
                cj.process();
                Console.WriteLine(fundName + "追加完成！");
            }
            Console.WriteLine("长江受托年金禁止池生成完成！");
            */
            #endregion 长江受托年金禁投池

            #region 工行受托年金
            /*
            //20170614，工行受托年金，使用文件：\\hfm-ywshare\ZXData\CaiHui\工行禁投。禁止池=禁止股票池，再各自追加各自的
            PoolProcessor gh;
            foreach (string fundName in "航天科技,福建联合石化,中国航空,中国核工业".Split(','))
            {
                gh = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\工行禁投\{0}\禁投股票{0}.XLS", strDate),
                    "",
                    DataSources.CONFIG,
                    fundName,
                    DataSources.getOutputFileName(outputPath, "工行受托年金禁止池_" + fundName));
                gh.process();
                Console.WriteLine(fundName + "追加完成！");
            }
            Console.WriteLine("工行受托年金禁止池生成完成！");
             */
            #endregion 工行受托年金

            #region 人寿年金
            /*
            RSAnnPoolProcessor rsann = new RSAnnPoolProcessor(
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\{0}\标准版{0}.XLS", strDate),
                "",   //人寿本身不筛选，马钢写ST
                DataSources.CONFIG, 
                "人寿受托年金禁投", 
                outputPath,
                ConfigurationManager.AppSettings["RSANN_LMT"],
                ConfigurationManager.AppSettings["RSANN_FBD"]);
            rsann.process();
            */
            #endregion 人寿年金

            #region 马钢年金
            /*
            RSAnnPoolProcessor rsann = new RSAnnPoolProcessor(
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\{0}\标准版{0}.XLS", strDate),
                "ST",   //人寿本身不筛选，马钢写ST
                @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm",
                "人寿受托年金禁投",
                outputPath,
                "马钢年金限制池",
                "31");
            rsann.processFbd();
            Console.WriteLine("马钢年金禁投处理完成！");

            //20171102，限投逻辑变更，不用人寿的processor了，直接筛选，很简单
            PoolProcessor mgLMT = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\{0}\标准版{0}.XLS", strDate),
                "5亿元|保留审计意见|无法表示审计意见|严重处罚|公开谴责",
                "",
                "",
                DataSources.getOutputFileName(outputPath, "人寿受托年金限投池_"));
            mgLMT.process();
            Console.WriteLine("马钢年金限投处理完成！");
            
            //马钢年金的限投，合并到人寿其中一个限投文件里，便于恒生使用
            SingleSheetCopyProcessor sscp = new SingleSheetCopyProcessor(DataSources.getOutputFileName(outputPath, "人寿受托年金限投池_")
                , "Sheet1"
                , string.Format(@"\\hfm-ywshare.js.local\ZXData\人寿托管年金禁限投股票池\池1\{0}\人寿受托年金限投池0_{0}.xlsx", strDate)
                , ConfigurationManager.AppSettings["RSANN_LMT"]);
            sscp.process();
            Console.WriteLine("马钢年金收益限投与平安限投合并完成！");
            //生成的这个文件重命名成"人寿受托年金限投池1_{0}.xls"，这个才是恒生要的文件
            File.Copy(string.Format(@"\\hfm-ywshare.js.local\ZXData\人寿托管年金禁限投股票池\池1\{0}\人寿受托年金限投池0_{0}.xlsx", strDate),
                string.Format(@"\\hfm-ywshare.js.local\ZXData\人寿托管年金禁限投股票池\池1\{0}\人寿受托年金限投池1_{0}.xlsx", strDate));
            */
            #endregion

            #region 建行受托年金禁投
            /*
            //红塔、唐钢、银河证券、国开行、航天科工、开滦、建行
            PoolProcessor jhnjFbd = new PoolProcessor(TpylPath, "",
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\{0}\标准版{0}.XLS", strDate), 
                string.Format("标准版{0}", strDate),
                DataSources.getOutputFileName(outputPath, "建行受托年金禁投标准文件temp"));
            jhnjFbd.process();

            jhnjFbd = new PoolProcessor(DataSources.getOutputFileName(outputPath, "建行受托年金禁投标准文件temp"), "保留审计意见|无法表示审计意见|1601|1602",
                "",
                "",
                DataSources.getOutputFileName(outputPath, "建行受托年金禁投标准文件"));
            jhnjFbd.process();

            foreach (string poolname in "红塔集团年金禁投,河北唐钢年金禁投,银河证券年金禁投,国开行年金禁投,开滦集团年金禁投,航天科工年金禁投,建行年金禁投,新疆农信年金禁投".Split(','))
            {
                jhnjFbd = new PoolProcessor(
                DataSources.getOutputFileName(outputPath, "建行受托年金禁投标准文件"),
                "",
                @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm",
                poolname,
                DataSources.getOutputFileName(outputPath, poolname)
                );
                jhnjFbd.process();
                Console.WriteLine(poolname + "生成完成！");
            }            

            File.Delete(DataSources.getOutputFileName(outputPath, "建行受托年金禁投标准文件temp"));
            File.Delete(DataSources.getOutputFileName(outputPath, "建行受托年金禁投标准文件"));
            */
            #endregion 建行受托年金禁投

            #region  北京农商行
            /*
            PoolProcessor tk1 = new PoolProcessor(TKPath_EQ, "", TKPath_FI, "HZQC",
                DataSources.getOutputFileName(outputPath, "泰康_temp"));            
            tk1.process();
            Console.WriteLine(DataSources.getOutputFileName(outputPath, "泰康_temp") + "生成完成！");

            PoolProcessor tk2 = new PoolProcessor(DataSources.getOutputFileName(outputPath, "泰康_temp"), 
                "", TKPath_OF, "HJJ",
                DataSources.getOutputFileName(outputPath, "泰康_temp1"));            
            tk2.process();            

            //追加
            PoolProcessor tk3 = new PoolProcessor(DataSources.getOutputFileName(outputPath, "泰康_temp1"), "",
                @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm", "北京农商行",
                DataSources.getOutputFileName(outputPath, "北京农商行黑名单"));
            tk3.process();

            Console.WriteLine(DataSources.getOutputFileName(outputPath, "北京农商行黑名单") + "生成完成！");

            File.Delete(DataSources.getOutputFileName(outputPath, "泰康_temp"));
            File.Delete(DataSources.getOutputFileName(outputPath, "泰康_temp1"));
            */
            #endregion  北京农商行

            #region 专户、长江年金限投、养老305、南方电网、中行年金、红塔限投
            /*
            //平安限投不需要做任何处理，直接拷过来
            File.Copy(PingAnLmtPath, outputPath + strDate + "\\平安委托帐户股票限投" + strDate + ".XLS");

            //平安限投这个需要按照恒生要求的格式做处理，把sheet复制几页
            SheetCopyProcessor scp = new SheetCopyProcessor(//DataSources.getOutputFileName(outputPath, "平安委托帐户股票限投"),
                outputPath + strDate + "\\平安委托帐户股票限投" + strDate + ".XLS",
                DataSources.getOutputFileName(outputPath, "平安委托帐户股票限投O32_temp_"),
                "专户平安人寿限制股票池;专户-平安人寿2号限制池;专户-平安人寿4号限制池;专户平安人寿3号限制池;专户平安产险1号限制池;专户平安产险2号限制池");
            scp.process();
            Console.WriteLine(DataSources.getOutputFileName(outputPath, "平安委托帐户股票限投O32_temp_") + "生成完成！");            

            
            PoolProcessor pinganAddRenbaoRenshou = new PoolProcessor(PingAnPath, DataSources.PINGAN_FILTER, DataSources.CONFIG, DataSources.RBRS_APPENDER,
                DataSources.getOutputFileName(outputPath, "平安数据源追加人保人寿禁投"));
            pinganAddRenbaoRenshou.process();
            Console.WriteLine(DataSources.getOutputFileName(outputPath, "平安数据源追加人保人寿禁投") + "生成完成！");
            
            //平安自己是不用filter的
            PoolProcessor pinganAddPingan = new PoolProcessor(PingAnPath, "", DataSources.CONFIG, DataSources.PINGAN_APPENDER,
                DataSources.getOutputFileName(outputPath, "平安数据源追加平安禁投"));
            pinganAddPingan.process();
            Console.WriteLine(DataSources.getOutputFileName(outputPath, "平安数据源追加平安禁投") + "生成完成！");

            
            
            //太平洋要处理两步，先追加太平洋禁投，再追加关联方禁投
            PoolProcessor pinganAddTpy = new PoolProcessor(PingAnPath, DataSources.PINGAN_FILTER, DataSources.CONFIG, DataSources.TPY_APPENDER,
                DataSources.getOutputFileName(outputPath, "平安数据源追加太平洋禁投"));
            pinganAddTpy.process();
            PoolProcessor pinganAddGlf = new PoolProcessor(DataSources.getOutputFileName(outputPath,"平安数据源追加太平洋禁投"), "", GlfPath, "太平洋人寿-关联方禁投池" + strDate,
                DataSources.getOutputFileName(outputPath,"平安数据源追加太平洋禁投追加关联方"));
            pinganAddGlf.process();
            Console.WriteLine(DataSources.getOutputFileName(outputPath,"平安数据源追加太平洋禁投追加关联方") + "生成完成！");
            File.Delete(DataSources.getOutputFileName(outputPath, "平安数据源追加太平洋禁投"));

            PoolProcessor pinganAddZgrsgsgs = new PoolProcessor(PingAnPath, DataSources.PINGAN_FILTER, DataSources.CONFIG, DataSources.ZGRSGSGS_APPENDER,
                DataSources.getOutputFileName(outputPath,"平安数据源追加中国人寿国寿固收禁投"));
            pinganAddZgrsgsgs.process();
            Console.WriteLine(DataSources.getOutputFileName(outputPath,"平安数据源追加中国人寿国寿固收禁投") + "生成完成！");

            PoolProcessor pinganAddGsjt = new PoolProcessor(PingAnPath, DataSources.PINGAN_FILTER, DataSources.CONFIG, DataSources.GSJT_APPENDER,
                DataSources.getOutputFileName(outputPath,"平安数据源追加国寿集团禁投"));
            pinganAddGsjt.process();
            Console.WriteLine(DataSources.getOutputFileName(outputPath,"平安数据源追加国寿集团禁投") + "生成完成！");
            
            //专户人寿限投，用平安的禁投，使用人寿的filter
            //需要两步，先生成平安+人寿filter文件
            RSLmtPoolProcessor pinganLmtFilter = new RSLmtPoolProcessor(PingAnPath, DataSources.PINGAN_FILTER, "", "",
                DataSources.getOutputFileName(outputPath, "专户人寿绝对收益限投"));
            pinganLmtFilter.process();
            Console.WriteLine(DataSources.getOutputFileName(outputPath, "专户人寿绝对收益限投") + "生成完成！");
            //按恒生导入限投池的方式，把专户人寿的限投合并到平安限投O32文件
            SingleSheetCopyProcessor sscp1 = new SingleSheetCopyProcessor(DataSources.getOutputFileName(outputPath, "专户人寿绝对收益限投")
                ,"Sheet1"
                , DataSources.getOutputFileName(outputPath, "平安委托帐户股票限投O32_temp_")
                , "专户人寿绝对收益限制池");
            sscp1.process();
            Console.WriteLine("专户人寿绝对收益限投与平安限投合并完成！");

            //20170607, 在O32那个文件里附加三个长江受托限投Sheet，长江金色交响年金限制池，上海烟草年金限制池，中国建筑材料年金限制池
            //源文件有个字段有问题，先预处理一下
            CJLmtPrePoolProcessor cjpp = new CJLmtPrePoolProcessor(string.Format(@"\\hfm-ywshare\ZXData\CaiHui\长江养老禁投\{0}\CJYLJTC_JYNEW{0}.xls", strDate)
                , "限投基金池"
                , Directory.GetCurrentDirectory() + "/copy.xls");
            cjpp.process();
            //数据源：\\hfm-ywshare\ZXData\CaiHui\长江养老禁投，限制基金池
            SingleSheetCopyProcessor sscp = new SingleSheetCopyProcessor(Directory.GetCurrentDirectory() + "/copy.xls"
                , "Sheet1"
                , DataSources.getOutputFileName(outputPath, "平安委托帐户股票限投O32_temp_")
                , "长江金色交响年金限制池");
            sscp.process();

            sscp = new SingleSheetCopyProcessor(Directory.GetCurrentDirectory() + "/copy.xls"
                , "Sheet1"
                , DataSources.getOutputFileName(outputPath, "平安委托帐户股票限投O32_temp_")
                , "上海烟草年金限制池");
            sscp.process();

            sscp = new SingleSheetCopyProcessor(Directory.GetCurrentDirectory() + "/copy.xls"
                , "Sheet1"
                , DataSources.getOutputFileName(outputPath, "平安委托帐户股票限投O32_temp_")
                , "中国建筑材料年金限制池");
            sscp.process();
            File.Delete(Directory.GetCurrentDirectory() + "/copy.xls");
            Console.WriteLine("长江受托年金限投合并完成！");

            //基本养老305组合限制股票池，使用数据 ：\\hfm-ywshare\ZXData\CaiHui\人寿养老禁投\20170714\标准版20170714.XLS，筛选备注为 “限制投资距投资时点12个月内受到证监会公开谴责或严重处罚的股票”
            //合并到平安委托帐户股票限投O32
            PoolProcessor yl305 = new PoolProcessor(string.Format(@"\\hfm-ywshare\ZXData\CaiHui\人寿养老禁投\{0}\标准版{0}.XLS", strDate)
                                        //, "限制投资距投资时点12个月内受到证监会公开谴责或严重处罚的股票"
                                        , "限制投资距投资时点12个月内受到证监会"
                                        , "", "",
                                        DataSources.getOutputFileName(outputPath, "基本养老保险305组合限制池"));
            yl305.process();
            Console.WriteLine(DataSources.getOutputFileName(outputPath, "基本养老保险305组合限制池") + "生成完成！");

            sscp = new SingleSheetCopyProcessor(DataSources.getOutputFileName(outputPath, "基本养老保险305组合限制池"),
                                                "Sheet1",
                                                DataSources.getOutputFileName(outputPath, "平安委托帐户股票限投O32_temp_"),
                                                "基本养老保险305组合限制池");
            sscp.process();
            Console.WriteLine("基本养老305限投合并完成！");
            
            //南方电网
            PoolProcessor nfdwFbd = new PoolProcessor(TpylPath, "1101|1102|1402|1601|1602|1605|1608", @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm","南方电网禁投",
                DataSources.getOutputFileName(outputPath, "南方电网禁投"));
            nfdwFbd.process();
            Console.WriteLine("南方电网禁投生成完成！");

            //南方电网的限投，用\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\20170920\标准版20170920.XLS筛选“限制投资股票已披露亏损”
            //合并\\hfm-ywshare.js.local\ZXData\CaiHui\嘉实基金_禁投池\20170920\下滑亏损扩大盈利变亏损20170920.XLS
            //合并\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\20170927\限投证券池(太平养老)20170927.XLS, 1404
            PoolProcessor nfdwLmt = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\{0}\标准版{0}.XLS", strDate), 
                //"限制投资股票已披露业绩预亏或亏损",
                "限制投资股票已披露亏损",
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\嘉实基金_禁投池\{0}\下滑亏损扩大盈利变亏损{0}.XLS", strDate),
                string.Format("下滑亏损扩大盈利变亏损{0}", strDate),
                DataSources.getOutputFileName(outputPath, "南方电网限投_temp_"));

            nfdwLmt.process();

            nfdwLmt = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\{0}\限投证券池(太平养老){0}.XLS", strDate),
                "1404",
                DataSources.getOutputFileName(outputPath, "南方电网限投_temp_"),
                "Sheet1",
                DataSources.getOutputFileName(outputPath, "南方电网限投"));

            nfdwLmt.process();

            sscp = new SingleSheetCopyProcessor(DataSources.getOutputFileName(outputPath, "南方电网限投"),
                                                "Sheet1",
                                                DataSources.getOutputFileName(outputPath, "平安委托帐户股票限投O32_temp_"),
                                                "南方电网限制池");
            sscp.process();

            File.Delete(DataSources.getOutputFileName(outputPath, "南方电网限投_temp_"));
            Console.WriteLine("南方电网限投生成完成！");

            //中行年金禁投
            PoolProcessor zhnjFbd = new PoolProcessor(TpylPath, "1601|1602", @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm", "中行年金禁投",
                DataSources.getOutputFileName(outputPath, "中行年金禁投"));
            zhnjFbd.process();
            Console.WriteLine("中行年金禁投生成完成！");

            //中行年金限投
            PoolProcessor zhnjLmt = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\{0}\标准版{0}.XLS", strDate),
                //"限制投资股票已披露业绩预亏或亏损",
                "保留审计意见|无法表示审计意见|限制投资股票已披露亏损",
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\嘉实基金_禁投池\{0}\下滑亏损扩大盈利变亏损{0}.XLS", strDate),
                string.Format("下滑亏损扩大盈利变亏损{0}", strDate),
                DataSources.getOutputFileName(outputPath, "中行年金限投_temp_"));

            zhnjLmt.process();

            sscp = new SingleSheetCopyProcessor(DataSources.getOutputFileName(outputPath, "中行年金限投_temp_"),
                                                "Sheet1",
                                                DataSources.getOutputFileName(outputPath, "平安委托帐户股票限投O32_temp_"),
                                                "中行年金限制池");
            sscp.process();

            File.Delete(DataSources.getOutputFileName(outputPath, "中行年金限投_temp_"));
            Console.WriteLine("中行年金限投生成完成！");

            //云南红塔限投
            PoolProcessor htLmt = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\{0}\标准版{0}.XLS", strDate),
                //"限制投资股票已披露业绩预亏或亏损",
                "限制投资股票已披露亏损",
                "",
                "",
                DataSources.getOutputFileName(outputPath, "云南红塔限投"));

            htLmt.process();

            sscp = new SingleSheetCopyProcessor(DataSources.getOutputFileName(outputPath, "云南红塔限投"),
                                                "Sheet1",
                                                DataSources.getOutputFileName(outputPath, "平安委托帐户股票限投O32_temp_"),
                                                "红塔集团限制池");
            sscp.process();

            Console.WriteLine("云南红塔限投生成完成！");

            //最后把文件重命名
            File.Copy(DataSources.getOutputFileName(outputPath, "平安委托帐户股票限投O32_temp_"),
                DataSources.getOutputFileName(outputPath, "平安委托帐户股票限投O32_"), true);
            */
            #endregion 专户、长江年金、养老305、南方电网、中行年金、红塔限投

            #region 中石油中石化
            
            //中石油禁投
            PoolProcessor zsyFbd = new PoolProcessor(TpylPath, "1601|1602|1605|1608|1402", 
                @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm", "中石油禁投",
                DataSources.getOutputFileName(outputPath, "中石油禁投_temp"));
            zsyFbd.process();
            zsyFbd = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\{0}\标准版{0}.XLS", strDate),
                //"限制投资股票已披露业绩预亏或亏损",
                "保留审计意见|无法表示审计意见",
                DataSources.getOutputFileName(outputPath, "中石油禁投_temp"),
                "Sheet1",
                DataSources.getOutputFileName(outputPath, "中石油禁投"));
            zsyFbd.process();
            File.Delete(DataSources.getOutputFileName(outputPath, "中石油禁投_temp"));

            Console.WriteLine("中石油禁投生成完成！");

            //中石化禁投
            PoolProcessor zshFbd = new PoolProcessor(TpylPath, "XXXX", //"1601|1602",
                @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm", "中石化禁投",
                DataSources.getOutputFileName(outputPath, "中石化禁投"));
            zshFbd.process();

            Console.WriteLine("中石化禁投生成完成！");

            //中石化限投
            // \\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\20170922\禁投证券池(太平养老)20170922.XLS，筛选1402
            //  合并\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\20170922\限投证券池(太平养老)20170922.XLS，筛选1404
            //  合并\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\20170922\标准版20170922.XLS，筛选“限制投资股票已披露亏损|限制投资股票已披露业绩预亏”
            PoolProcessor zshLmt = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\{0}\禁投证券池(太平养老){0}.XLS", strDate),
                "",
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\{0}\限投证券池(太平养老){0}.XLS", strDate),
                string.Format("限投证券池(太平养老){0}", strDate),
                DataSources.getOutputFileName(outputPath, "中石化限投_temp1_"));
            zshLmt.process();

            zshLmt = new PoolProcessor(DataSources.getOutputFileName(outputPath, "中石化限投_temp1_"),
                "",
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\{0}\标准版{0}.XLS", strDate),
                string.Format("标准版{0}", strDate),
                DataSources.getOutputFileName(outputPath, "中石化限投_temp2_"));
            zshLmt.process();

            zshLmt = new PoolProcessor(DataSources.getOutputFileName(outputPath, "中石化限投_temp2_"),
                "1402|1404|限制投资股票已披露亏损|限制投资股票已披露业绩预亏",
                null,
                null,
                DataSources.getOutputFileName(outputPath, "中石化限投"));
            zshLmt.process();

            Console.WriteLine("中石化限投生成完成！");
            
            #endregion 中石油中石化
              
            #region 基本养老903禁投
            /*
            //基本养老903禁投，这个处理方式比较特殊，单独弄个processor            
            TPYLPoolProcessor tpyl = new TPYLPoolProcessor(TpylPath, "1601|1602", "", "",
                DataSources.getOutputFileName(outputPath, "基本养老903组合限投股票池"));
            tpyl.process();
            Console.WriteLine(DataSources.getOutputFileName(outputPath, "基本养老903组合限投股票池") + "生成完成！");
            //侯坤要求，903生成好后发邮件
            
            SendMail.sendEMailThroughOUTLOOK("基本养老903组合限投股票池" + strDate,
                " ",
                DataSources.getOutputFileName(outputPath, "基本养老903组合限投股票池"),
                "luocw@jsfund.cn;liubin01@jsfund.cn;longcl@jsfund.cn;luoqiong@jsfund.cn;houkun@jsfund.cn"
                );
            */           
            #endregion 基本养老903禁投

            #region 基本养老807限投
            /*
            PoolProcessor yl807 = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\TianXiang\{0}\jsjj-sbxt-{0}.xls", strDate),
                "公开谴责|违规处罚|关联方", "", "",
                DataSources.getOutputFileName(outputPath, "基本养老807组合限投股票池"));
            yl807.process();
            Console.WriteLine(DataSources.getOutputFileName(outputPath, "基本养老807组合限投股票池") + "生成完成！");
            */
            #endregion

            #region 东风汽车
            /*
            //禁投：人寿备注：保留审计意见|无法表示审计意见|严重处罚|公开谴责，\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\20180102\标准版20180102.XLS
            //      平安限制股票：涉嫌违法
            //      平安禁止股票：调查和处罚
            //      平安禁止债券：延迟支付
            //      关联方
            PoolProcessor dfqc = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\{0}\标准版{0}.XLS", strDate),
                "审计意见|严重处罚|公开谴责",
                @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm",
                "东风汽车",
                DataSources.getOutputFileName(outputPath, "东风汽车禁投temp1_"));
            dfqc.process();

            //平安年金的文件有点问题，代码和名称两列反了，注释列C/E也是反的，交换一下
            Excel.Workbook temp_wbk = DataSources.ExcelApp.Workbooks.Open(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\平安年金\{0}\限投股票_{0}.XLS", strDate));
            Excel.Worksheet temp_sheet = temp_wbk.Worksheets[1];
            Excel.Range copyRange = temp_sheet.Range["B:B"];
            Excel.Range insertRange = temp_sheet.Range["A:A"];
            insertRange.Insert(Excel.XlInsertShiftDirection.xlShiftToRight, copyRange.Cut());
            copyRange = temp_sheet.Range["C:C"];
            insertRange = temp_sheet.Range["E:E"];
            insertRange.Insert(Excel.XlInsertShiftDirection.xlShiftToRight, copyRange.Copy());
            temp_wbk.SaveAs(outputPath + strDate + @"\平安年金限投股票调整.XLS");
            Console.WriteLine(outputPath + strDate + @"\平安年金限投股票调整.XLS生成完成！");

            dfqc = new PoolProcessor(outputPath + strDate + @"\平安年金限投股票调整.XLS",
                "涉嫌违法",
                DataSources.getOutputFileName(outputPath, "东风汽车禁投temp1_"),
                "Sheet1",
                DataSources.getOutputFileName(outputPath, "东风汽车禁投temp2_"));
            dfqc.process();

            //平安年金的文件有点问题，代码和名称两列反了，交换一下
            temp_wbk = DataSources.ExcelApp.Workbooks.Open(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\平安年金\{0}\禁投股票_{0}.XLS", strDate));
            temp_sheet = temp_wbk.Worksheets[1];
            copyRange = temp_sheet.Range["B:B"];
            insertRange = temp_sheet.Range["A:A"];
            insertRange.Insert(Excel.XlInsertShiftDirection.xlShiftToRight, copyRange.Cut());
            copyRange = temp_sheet.Range["C:C"];
            insertRange = temp_sheet.Range["E:E"];
            insertRange.Insert(Excel.XlInsertShiftDirection.xlShiftToRight, copyRange.Copy());
            temp_wbk.SaveAs(outputPath + strDate + @"\平安年金禁投股票调整.XLS");
            Console.WriteLine(outputPath + strDate + @"\平安年金禁投股票调整.XLS生成完成！");

            dfqc = new PoolProcessor(outputPath + strDate + @"\平安年金禁投股票调整.XLS",
                "调查和处罚",
                DataSources.getOutputFileName(outputPath, "东风汽车禁投temp2_"),
                "Sheet1",
                DataSources.getOutputFileName(outputPath, "东风汽车禁投temp3_"));
            dfqc.process();

            //平安年金的文件有点问题，代码和名称两列反了，交换一下
            temp_wbk = DataSources.ExcelApp.Workbooks.Open(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\平安年金\{0}\禁投债券_{0}.XLS", strDate));
            temp_sheet = temp_wbk.Worksheets[1];
            copyRange = temp_sheet.Range["B:B"];
            insertRange = temp_sheet.Range["A:A"];
            insertRange.Insert(Excel.XlInsertShiftDirection.xlShiftToRight, copyRange.Cut());
            copyRange = temp_sheet.Range["C:C"];
            insertRange = temp_sheet.Range["E:E"];
            insertRange.Insert(Excel.XlInsertShiftDirection.xlShiftToRight, copyRange.Copy());
            temp_wbk.SaveAs(outputPath + strDate + @"\平安年金禁投债券调整.XLS");
            Console.WriteLine(outputPath + strDate + @"\平安年金禁投债券调整.XLS生成完成！");

            dfqc = new PoolProcessor(outputPath + strDate + @"\平安年金禁投债券调整.XLS",
                "延迟支付",
                DataSources.getOutputFileName(outputPath, "东风汽车禁投temp3_"),
                "Sheet1",
                DataSources.getOutputFileName(outputPath, "东风汽车禁投"));
            dfqc.process();

            Console.WriteLine(DataSources.getOutputFileName(outputPath, "东风汽车禁投") + "生成完成");
            
            //限投：平安限制股票：亏损，监管部门
            //      平安限制债券：评级，行业
            //      人寿：亏损、预亏
            //平安年金的文件有点问题，代码和名称两列反了，注释列C/E也是反的，交换一下
            temp_wbk = DataSources.ExcelApp.Workbooks.Open(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\平安年金\{0}\限投债券_{0}.XLS", strDate));
            temp_sheet = temp_wbk.Worksheets[1];
            copyRange = temp_sheet.Range["B:B"];
            insertRange = temp_sheet.Range["A:A"];
            insertRange.Insert(Excel.XlInsertShiftDirection.xlShiftToRight, copyRange.Cut());
            copyRange = temp_sheet.Range["C:C"];
            insertRange = temp_sheet.Range["E:E"];
            insertRange.Insert(Excel.XlInsertShiftDirection.xlShiftToRight, copyRange.Copy());
            temp_wbk.SaveAs(outputPath + strDate + @"\平安年金限投债券调整.XLS");
            Console.WriteLine(outputPath + strDate + @"\平安年金限投债券调整.XLS生成完成！");

            dfqc = new PoolProcessor(outputPath + strDate + @"\平安年金限投债券调整.XLS",
                "评级|行业",
                "",
                "",
                DataSources.getOutputFileName(outputPath, "东风汽车限投temp1"));
            dfqc.process();

            dfqc = new PoolProcessor(outputPath + strDate + @"\平安年金限投股票调整.XLS",
                "亏损|监管部门",
                DataSources.getOutputFileName(outputPath, "东风汽车限投temp1"),
                "Sheet1",
                DataSources.getOutputFileName(outputPath, "东风汽车限投temp2"));
            dfqc.process();

            dfqc = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\{0}\标准版{0}.XLS", strDate),
                "亏损|预亏",
                DataSources.getOutputFileName(outputPath, "东风汽车限投temp2"),
                "Sheet1",
                DataSources.getOutputFileName(outputPath, "东风汽车限投"));
            dfqc.process();
            Console.WriteLine(DataSources.getOutputFileName(outputPath, "东风汽车限投") + "生成完成");
            */
            #endregion 东风汽车

            #region 浙电力部属
            /*
            //浙江电力部属禁投池，要求如下：
            //1. \\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\20170712\禁投证券池(太平养老)20170712.XLS，筛选1102	 ST，1605 立案调查，1601	处罚，1602 谴责
            //2. \\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\20170712\限投证券池(太平养老)20170712.XLS，筛选1404	 会计意见
            //3. \\hfm-ywshare.js.local\ZXData\CaiHui\嘉实基金_禁投池\20170712\下滑亏损扩大盈利变亏损20170712.XLS
            //4. \\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\20170712\标准版20170712.XLS，筛选限制投资股票已披露业绩预亏或亏损
            //由于source可以过滤，appender不能过滤，因此实现顺序如下：
            //1追加3，生成temp_1；2追加temp_1，生成temp_2;4追加temp_2生成temp_3;temp_3追加关联方工商银行生成最终文件
            PoolProcessor zjdl = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\{0}\禁投证券池(太平养老){0}.XLS", strDate),
                                                "1102|1605|1601|1602",
                                                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\嘉实基金_禁投池\{0}\下滑亏损扩大盈利变亏损{0}.XLS", strDate),
                                                string.Format("下滑亏损扩大盈利变亏损{0}", strDate),
                                                DataSources.getOutputFileName(outputPath, "浙江电力部属禁投池temp_1"));
            zjdl.process();
            Console.WriteLine(DataSources.getOutputFileName(outputPath, "浙江电力部属禁投池temp_1") + "生成完成！");

            zjdl = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\{0}\限投证券池(太平养老){0}.XLS", strDate),
                                    "1404",
                                    DataSources.getOutputFileName(outputPath, "浙江电力部属禁投池temp_1"),
                                    "Sheet1",
                                    DataSources.getOutputFileName(outputPath, "浙江电力部属禁投池temp_2"));
            zjdl.process();
            Console.WriteLine(DataSources.getOutputFileName(outputPath, "浙江电力部属禁投池temp_2") + "生成完成！");

            zjdl = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\{0}\标准版{0}.XLS", strDate),
                                    "限制投资股票已披露亏损",
                                    DataSources.getOutputFileName(outputPath, "浙江电力部属禁投池temp_2"), 
                                    "Sheet1",
                                    DataSources.getOutputFileName(outputPath, "浙江电力部属禁投池temp_3"));
            zjdl.process();
            Console.WriteLine(DataSources.getOutputFileName(outputPath, "浙江电力部属禁投池temp_3") + "生成完成！");

            zjdl = new PoolProcessor(DataSources.getOutputFileName(outputPath, "浙江电力部属禁投池temp_3"),
                                    "",
                                    @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm",
                                    @"浙电力部属",
                                    DataSources.getOutputFileName(outputPath, "浙江电力部属禁投池"));
            zjdl.process();
            Console.WriteLine(DataSources.getOutputFileName(outputPath, "浙江电力部属禁投池") + "生成完成！");


            File.Delete(DataSources.getOutputFileName(outputPath, "浙江电力部属禁投池temp_1"));
            File.Delete(DataSources.getOutputFileName(outputPath, "浙江电力部属禁投池temp_2"));
            File.Delete(DataSources.getOutputFileName(outputPath, "浙江电力部属禁投池temp_3"));
            */
            #endregion 浙电力部属

            #region 平安年金
            /*
            //禁投：\\hfm-ywshare\ZXData\CaiHui\平安年金\20170801\禁投股票_20170801  与禁止池\\hfm-ywshare\ZXData\CaiHui\平安年金\20170801\禁投债券_20170801 整合
            //限投：\\hfm-ywshare\ZXData\CaiHui\平安年金\20170801\限投债券_20170801 与限制池 \\hfm-ywshare\ZXData\CaiHui\平安年金\20170801\限投股票_20170801 整合
            //同时禁投各自追加股票\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm 平安年金禁投
            //1. 禁投 - 两个文件合并
            PoolProcessor pp1 = new PoolProcessor(string.Format(@"\\hfm-ywshare\ZXData\CaiHui\平安年金\{0}\禁投股票_{0}.XLS", strDate), "",
                string.Format(@"\\hfm-ywshare\ZXData\CaiHui\平安年金\{0}\禁投债券_{0}.XLS", strDate),
                "Sheet1",
                DataSources.getOutputFileName(outputPath, "平安年金禁投通用"));
            pp1.process();

            //平安年金的文件有点问题，代码和名称两列反了，交换一下
            Excel.Workbook temp_wbk = DataSources.ExcelApp.Workbooks.Open(DataSources.getOutputFileName(outputPath, "平安年金禁投通用"));
            Excel.Worksheet temp_sheet = temp_wbk.Worksheets[1];
            Excel.Range copyRange = temp_sheet.Range["B:B"];
            Excel.Range insertRange = temp_sheet.Range["A:A"];
            insertRange.Insert(Excel.XlInsertShiftDirection.xlShiftToRight, copyRange.Cut());
            temp_wbk.Save();
            Console.WriteLine(DataSources.getOutputFileName(outputPath, "平安年金禁投通用") + "生成完成！");

            //2. 各自追加禁投股票            
            AppendPoolProcessor app = new AppendPoolProcessor(DataSources.getOutputFileName(outputPath, "平安年金禁投通用"),
                "", @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm",
                "平安年金禁投",
                outputPath,
                "24;46;67;82;85;89;138;263;171;178;228;233;238;282;291;350;424;562;69");
            app.process();
            Console.WriteLine("平安年金禁投各自追加" + "生成完成！");

            //3. 限投 - 两个文件合并
            pp1 = new PoolProcessor(string.Format(@"\\hfm-ywshare\ZXData\CaiHui\平安年金\{0}\限投股票_{0}.XLS", strDate), "",
                string.Format(@"\\hfm-ywshare\ZXData\CaiHui\平安年金\{0}\限投债券_{0}.XLS", strDate),
                "Sheet1",
                DataSources.getOutputFileName(outputPath, "平安年金限投通用"));
            pp1.process();

            //平安年金的文件有点问题，代码和名称两列反了，交换一下
            temp_wbk = DataSources.ExcelApp.Workbooks.Open(DataSources.getOutputFileName(outputPath, "平安年金限投通用"));
            temp_sheet = temp_wbk.Worksheets[1];
            copyRange = temp_sheet.Range["B:B"];
            insertRange = temp_sheet.Range["A:A"];
            insertRange.Insert(Excel.XlInsertShiftDirection.xlShiftToRight, copyRange.Cut());
            temp_wbk.Save();
            Console.WriteLine("平安年金限投通用生成完成！");                      

            //4. 限投 - Sheet复制多份
            SheetCopyProcessor scp = new SheetCopyProcessor(DataSources.getOutputFileName(outputPath, "平安年金限投通用"),
                DataSources.getOutputFileName(outputPath, "平安年金限投"),
                "厦门卷烟限制池;红河烟草限制池;哈电集团限制池;安徽烟草（肥外）限制池;厦门港务年金限制池;中钢集团限制池;东莞银行年金限制池;吉林电力年金限制池;中冶科工年金限制池;华侨城年金限制池;中国电信集团年金限制池;中国储备粮年金限制池;中煤能源集团年金限制池;中国建筑年金限制池;中国邮储银行年金限制池;中国移动年金限制池;中国船舶工业集团年金限制池;深圳路桥限制池;碧辟中国公司限制池");
            scp.process();
            Console.WriteLine("平安年金限投生成完成！");
            */
            #endregion 平安年金

            #region 太平年金
            /*
            //禁止池使用文件 \\hfm-ywshare\ZXData\CaiHui\太平养老禁投\20170804\禁投证券池(太平养老)20170804.XLS
            //限制池使用文件 \\hfm-ywshare\ZXData\CaiHui\太平养老禁投\20170804\限投证券池(太平养老)20170804.XLS
            //同时禁投各自追加股票\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm 太平年金禁投
            //1. 禁投
            AppendPoolProcessor app = new AppendPoolProcessor(string.Format(@"\\hfm-ywshare\ZXData\CaiHui\太平养老禁投\{0}\禁投证券池(太平养老){0}.XLS", strDate),
                "", @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm",
                "太平年金禁投",
                outputPath,
                "33;38;49;50;214;221;259;297;323");
            app.process();
            Console.WriteLine("太平年金禁投生成完成！");

            //2. 限投
            SheetCopyProcessor scp = new SheetCopyProcessor(string.Format(@"\\hfm-ywshare\ZXData\CaiHui\太平养老禁投\{0}\限投证券池(太平养老){0}.XLS", strDate),
                DataSources.getOutputFileName(outputPath, "太平年金限投temp"),
                "徐矿年金限制池;京能热电限制池;西门子增值限制池;西门子稳健限制池;广东交通集团年金限制池;天津电力年金限制池;深圳能源年金限制池;内蒙古东部电力年金限制池;中国证券业协会年金限制池");
            scp.process();
            Console.WriteLine("太平年金限投生成完成！");

            //20180122, 南航单独处理禁投池：南方航空年金禁投（附件Sheet1）+禁投证券池（太平养老）1101、1102、1402、1601、1602、1605、1608+中国银行（601988）+南方航空（600029）
            //限投池：南方航空年金限投（附件Sheet2）+限投证券池（太平养老）1404、1508
            PoolProcessor pp = new PoolProcessor(string.Format(@"\\hfm-ywshare\ZXData\CaiHui\太平养老禁投\{0}\禁投证券池(太平养老){0}.XLS", strDate),
                "1101|1102|1402|1601|1602|1605|1608",
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\{0}\南方航空年金禁投{0}.XLS", strDate),
                string.Format("南方航空年金禁投{0}", strDate),
                DataSources.getOutputFileName(outputPath, "南航禁投temp"));
            pp.process();
            pp = new PoolProcessor(DataSources.getOutputFileName(outputPath, "南航禁投temp"),
                "",
                @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm",
                "南方航空",
                DataSources.getOutputFileName(outputPath, "禁投池_311_"));
            pp.process();
            Console.WriteLine("南航禁投生成完成！");

            pp = new PoolProcessor(string.Format(@"\\hfm-ywshare\ZXData\CaiHui\太平养老禁投\{0}\限投证券池(太平养老){0}.XLS", strDate),
                "1404|1508",
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\{0}\南方航空年金限投{0}.XLS", strDate),
                string.Format("南方航空年金限投{0}", strDate),
                DataSources.getOutputFileName(outputPath, "南航限投temp"));
            pp.process();
            Console.WriteLine("南航限投生成完成！");

            SingleSheetCopyProcessor sscp = new SingleSheetCopyProcessor(DataSources.getOutputFileName(outputPath, "南航限投temp"), "Sheet1",
                DataSources.getOutputFileName(outputPath, "太平年金限投temp"),
                "南航集团年金限制池");
            sscp.process();
            File.Copy(DataSources.getOutputFileName(outputPath, "太平年金限投temp"), DataSources.getOutputFileName(outputPath, "太平年金限投"));
            Console.WriteLine("南航限投合并完成！");
            */
            #endregion 太平年金

            #region 沈阳铁路局、北京铁路局
            /*
            //禁投：\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\20170922\禁投证券池(太平养老)20170922.XLS，筛选1101/1102、1605、1601/1602、1402
            //  合并\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\20170922\限投证券池(太平养老)20170922.XLS，筛选1404
            //  追加配置文件里的“沈阳铁路局”
            //限投：\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\20170922\标准版20170922.XLS，筛选“限制投资股票已披露亏损”
            //  合并\\hfm-ywshare.js.local\ZXData\CaiHui\嘉实基金_禁投池\20170922\下滑亏损扩大盈利变亏损20170922.XLS
            
            //处理禁投
            PoolProcessor tlj = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\{0}\禁投证券池(太平养老){0}.XLS", strDate),
                "",
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\{0}\限投证券池(太平养老){0}.XLS", strDate),
                string.Format("限投证券池(太平养老){0}", strDate),
                DataSources.getOutputFileName(outputPath, "沈阳北京铁路局_temp1_"));
            tlj.process();
            tlj = new PoolProcessor(DataSources.getOutputFileName(outputPath, "沈阳北京铁路局_temp1_"),
                "1101|1102|1605|1601|1602|1402|1404",
                @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm",
                "沈阳铁路局",
                DataSources.getOutputFileName(outputPath, "沈阳北京铁路局禁投"));
            tlj.process();
            
            File.Delete(DataSources.getOutputFileName(outputPath, "沈阳北京铁路局_temp1_"));
            Console.WriteLine("沈阳北京铁路局禁投生成完成！");

            //处理限投
            tlj = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\{0}\标准版{0}.XLS", strDate),
                "限制投资股票已披露亏损",
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\嘉实基金_禁投池\{0}\下滑亏损扩大盈利变亏损{0}.XLS", strDate),
                string.Format("下滑亏损扩大盈利变亏损{0}", strDate),
                DataSources.getOutputFileName(outputPath, "沈阳北京铁路局限投"));
            tlj.process();
            Console.WriteLine("沈阳北京铁路局限投生成完成！");
            */
            #endregion 沈阳铁路局、北京铁路局

            #region 南昌铁路局
            /*
            //禁投：\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\20170922\禁投证券池(太平养老)20170922.XLS，筛选1101/1102、1601/1602、1402
            //  合并\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\20170922\限投证券池(太平养老)20170922.XLS，筛选1404
            //  追加配置文件里的“南昌铁路局”

            //处理禁投
            PoolProcessor tlj = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\{0}\禁投证券池(太平养老){0}.XLS", strDate),
                "",
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\{0}\限投证券池(太平养老){0}.XLS", strDate),
                string.Format("限投证券池(太平养老){0}", strDate),
                DataSources.getOutputFileName(outputPath, "南昌铁路局_temp1_"));
            tlj.process();
            tlj = new PoolProcessor(DataSources.getOutputFileName(outputPath, "南昌铁路局_temp1_"),
                "1101|1102|1601|1602|1402|1404",
                @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm",
                "南昌铁路局",
                DataSources.getOutputFileName(outputPath, "南昌铁路局禁投"));
            tlj.process();

            File.Delete(DataSources.getOutputFileName(outputPath, "南昌铁路局_temp1_"));
            Console.WriteLine("南昌铁路局禁投生成完成！");
             * */
            #endregion  南昌铁路局

            #region 上海铁路局、呼市铁路局
            /*
            //禁投：\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\20170922\禁投证券池(太平养老)20170922.XLS，筛选1101/1102、1601/1602、1402
            //  合并\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\20170922\限投证券池(太平养老)20170922.XLS，筛选1404
            //  合并\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\20170922\标准版20170922.XLS，筛选“限制投资股票已披露亏损”
            //  合并\\hfm-ywshare.js.local\ZXData\CaiHui\嘉实基金_禁投池\20170922\下滑亏损扩大盈利变亏损20170922.XLS
            //  追加配置文件里的“上海铁路局”，“呼市铁路局”
            
            //处理禁投
            PoolProcessor tlj = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\{0}\禁投证券池(太平养老){0}.XLS", strDate),
                "",
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\{0}\限投证券池(太平养老){0}.XLS", strDate),
                string.Format("限投证券池(太平养老){0}", strDate),
                DataSources.getOutputFileName(outputPath, "上海呼市铁路局_temp1_"));
            tlj.process();

            tlj = new PoolProcessor(DataSources.getOutputFileName(outputPath, "上海呼市铁路局_temp1_"),
                "",
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\{0}\标准版{0}.XLS", strDate),
                string.Format("标准版{0}", strDate),
                DataSources.getOutputFileName(outputPath, "上海呼市铁路局_temp2_"));
            tlj.process();           

            tlj = new PoolProcessor(DataSources.getOutputFileName(outputPath, "上海呼市铁路局_temp2_"),
                "1101|1102|1601|1602|1402|1404|限制投资股票已披露亏损",
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\嘉实基金_禁投池\{0}\下滑亏损扩大盈利变亏损{0}.XLS", strDate),
                string.Format("下滑亏损扩大盈利变亏损{0}", strDate),
                DataSources.getOutputFileName(outputPath, "上海呼市铁路局_temp3_"));
            tlj.process();

            tlj = new PoolProcessor(DataSources.getOutputFileName(outputPath, "上海呼市铁路局_temp3_"),
               "",
               @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm",
               "呼市铁路局",    //!!!!!!上海呼市铁路局唯一区别就是这个
                DataSources.getOutputFileName(outputPath, "上海呼市铁路局禁投"));
            tlj.process();

            File.Delete(DataSources.getOutputFileName(outputPath, "上海呼市铁路局_temp1_"));
            File.Delete(DataSources.getOutputFileName(outputPath, "上海呼市铁路局_temp2_"));
            File.Delete(DataSources.getOutputFileName(outputPath, "上海呼市铁路局_temp3_"));
            Console.WriteLine("上海呼市铁路局禁投生成完成！");           
            */
            #endregion 上海铁路局、呼市铁路局

            #region 昆明铁路局
            /*
            //禁投：\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\20170922\禁投证券池(太平养老)20170922.XLS，筛选1101/1102、1601/1602/1611
            //  追加配置文件里的“昆明铁路局”
            //限投：\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\20170922\禁投证券池(太平养老)20170922.XLS，筛选1402
            //  合并\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\20170922\限投证券池(太平养老)20170922.XLS，筛选1404/1508
            //  合并\\hfm-ywshare.js.local\ZXData\CaiHui\嘉实基金_禁投池\20170922\下滑亏损扩大盈利变亏损20170922.XLS
            
            //处理禁投
            PoolProcessor kmtlj = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\{0}\禁投证券池(太平养老){0}.XLS", strDate),
                "1101|1102|1601|1602|1611",
                @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm",
               "昆明铁路局",   
                DataSources.getOutputFileName(outputPath, "昆明铁路局禁投"));
            kmtlj.process();
            Console.WriteLine("昆明铁路局禁投生成完成！");    

            //处理限投
            PoolProcessor tlj = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\{0}\禁投证券池(太平养老){0}.XLS", strDate),
                "",
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\{0}\限投证券池(太平养老){0}.XLS", strDate),
                string.Format("限投证券池(太平养老){0}", strDate),
                DataSources.getOutputFileName(outputPath, "昆明铁路局限投_temp1_"));
            tlj.process();

            tlj = new PoolProcessor(DataSources.getOutputFileName(outputPath, "昆明铁路局限投_temp1_"),
                "1508|1402|1404|限制投资股票已披露亏损",
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\嘉实基金_禁投池\{0}\下滑亏损扩大盈利变亏损{0}.XLS", strDate),
                string.Format("下滑亏损扩大盈利变亏损{0}", strDate),
                DataSources.getOutputFileName(outputPath, "昆明铁路局限投"));
            tlj.process();
            Console.WriteLine("昆明铁路局限投生成完成！"); 
            */
            #endregion 昆明铁路局

            #region 13支年金禁限投
            /*
            //谴责处罚，太平筛选1601，1602
            //审计意见（人寿标准版本 备注“限制投资最近一年度财务报表被会计师事务所出具保留审计意见的公司股票"+"限制投资最近一年度财务报表被会计师事务所出具无法表示审计意见的公司股票"）
            //亏损下滑（人寿标准版本 备注“限制投资股票已披露亏损"+嘉实定制“下滑亏损扩大盈利变亏损”）

            //1. 上面三个大合并，注意没有包含嘉实那个
            PoolProcessor tp = new PoolProcessor(string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\太平养老禁投\{0}\禁投证券池(太平养老){0}.XLS", strDate),
                "",
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\{0}\标准版{0}.XLS", strDate),
                string.Format("标准版{0}", strDate),
                DataSources.getOutputFileName(outputPath, "大模板"));
            tp.process();

            //2. 谴责处罚+审计意见+亏损下滑
            tp = new PoolProcessor(DataSources.getOutputFileName(outputPath, "大模板"),
                "1601|1602|保留审计意见|无法表示审计意见|限制投资股票已披露亏损",
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\嘉实基金_禁投池\{0}\下滑亏损扩大盈利变亏损{0}.XLS", strDate),
                string.Format("下滑亏损扩大盈利变亏损{0}", strDate), 
                DataSources.getOutputFileName(outputPath, "谴责处罚审计意见亏损下滑"));
            tp.process();
            Console.WriteLine("谴责处罚审计意见亏损下滑生成完成！");
            //3. 谴责处罚+审计意见
            tp = new PoolProcessor(DataSources.getOutputFileName(outputPath, "谴责处罚审计意见亏损下滑"),
                "1601|1602|保留审计意见|无法表示审计意见", "", "", DataSources.getOutputFileName(outputPath, "谴责处罚审计意见"));
            tp.process();
            Console.WriteLine("谴责处罚审计意见生成完成！");
            //4. 审计意见+亏损下滑
            tp = new PoolProcessor(DataSources.getOutputFileName(outputPath, "谴责处罚审计意见亏损下滑"),
                "保留审计意见|无法表示审计意见|限制投资股票已披露亏损", 
                string.Format(@"\\hfm-ywshare.js.local\ZXData\CaiHui\嘉实基金_禁投池\{0}\下滑亏损扩大盈利变亏损{0}.XLS", strDate),
                string.Format("下滑亏损扩大盈利变亏损{0}", strDate),
                DataSources.getOutputFileName(outputPath, "审计意见亏损下滑"));
            tp.process();
            Console.WriteLine("审计意见亏损下滑生成完成！");
            //5. 谴责处罚
            tp = new PoolProcessor(DataSources.getOutputFileName(outputPath, "谴责处罚审计意见"),
                "1601|1602", "", "", DataSources.getOutputFileName(outputPath, "谴责处罚"));
            tp.process();
            Console.WriteLine("谴责处罚生成完成！");
            
            //各自追加
            AppendPoolProcessor app = new AppendPoolProcessor(DataSources.getOutputFileName(outputPath, "谴责处罚审计意见亏损下滑"),
                "", @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm",
                "其他年金禁投",
                outputPath,
                "招行金色;中邮器材");
            app.process();

            app = new AppendPoolProcessor(DataSources.getOutputFileName(outputPath, "谴责处罚审计意见"),
                "", @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm",
                "其他年金禁投",
                outputPath,
                "晋煤;中诚;中信集团;工行年金;工行专户");
            app.process();

            app = new AppendPoolProcessor(DataSources.getOutputFileName(outputPath, "谴责处罚"),
                "", @"\\hfm-ywshare.js.local\ZXData\禁限投数据处理工具20170330_侯坤.xlsm",
                "其他年金禁投",
                outputPath,
                "潞安矿业;山西焦煤;铁三院;中铁快运;招行年金");
            app.process();
            */
            #endregion 13支年金禁限投

            //清理资源
            DataSources.ExcelApp.Quit();
            System.Runtime.InteropServices.Marshal.ReleaseComObject(DataSources.ExcelApp);
        }
    }
}

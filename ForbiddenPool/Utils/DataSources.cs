﻿using System;
using System.Collections.Generic;

using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Configuration;  

namespace cn.jsfund.compliance.Utils
{
    public class DataSources
    {
        public static Excel.Application ExcelApp = new Excel.Application();
        public static String CONFIG = ConfigurationManager.AppSettings["CONFIG"];
        public static String OUTPUT_PATH = ConfigurationManager.AppSettings["OUTPUT_PATH"];

        //平安禁投池及筛选条件
        public static String PINGAN_FBD = ConfigurationManager.AppSettings["PINGAN_FBD"];
        public static String PINGAN_FILTER = ConfigurationManager.AppSettings["PINGAN_FILTER"];
        //关联方禁投池
        public static String GLF_FBD = ConfigurationManager.AppSettings["GLF_FBD"];

        //人保、人寿禁投追加Sheet页名称
        public static string RBRS_APPENDER = ConfigurationManager.AppSettings["RBRS_APPENDER"];
        //平安禁投追加Sheet页名称
        public static string PINGAN_APPENDER = ConfigurationManager.AppSettings["PINGAN_APPENDER"];
        //太平洋禁投追加Sheet页名称
        public static string TPY_APPENDER = ConfigurationManager.AppSettings["TPY_APPENDER"];
        //中国人寿、国寿固收禁投追加Sheet页名称
        public static string ZGRSGSGS_APPENDER = ConfigurationManager.AppSettings["ZGRSGSGS_APPENDER"];
        //国寿集团禁投追加Sheet页名称
        public static string GSJT_APPENDER = ConfigurationManager.AppSettings["GSJT_APPENDER"];

        //人寿限投有专门的filter，保留得更多
        public static String RS_LMT_FILTER = ConfigurationManager.AppSettings["RS_LMT_FILTER"];

        //太平养老禁投池
        public static string TPYL_FBD = ConfigurationManager.AppSettings["TPYL_FBD"];

        //人寿养老禁投
        public static string RS_FBD = ConfigurationManager.AppSettings["RS_FBD"];
        public static string RS_FILTER = ConfigurationManager.AppSettings["RS_FILTER"];

        //泰康养老禁限投目录
        public static string TK_FBD = ConfigurationManager.AppSettings["TK_FBD"];

        public static string getOutputFileName(string outputPath, string baseName)
        {
            return outputPath + System.DateTime.Now.ToString("yyyyMMdd") + "\\" + baseName + System.DateTime.Now.ToString("yyyyMMdd") + ".xlsx";    
        }
    }
}

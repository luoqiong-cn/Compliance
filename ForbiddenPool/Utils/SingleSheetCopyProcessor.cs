﻿using System;
using System.Collections.Generic;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;
using cn.jsfund.compliance.Utils;

namespace cn.jsfund.compliance.Utils
{
    /// <summary>
    /// 这个processor用于把source里的第一个sheet复制到目标文件
    /// </summary>
    public class SingleSheetCopyProcessor
    {
        private string dataSource { get; set; }
        private string srcSheet { get; set; }
        /// <summary>
        /// 必须是一个已经存在的文件
        /// </summary>
        private string outputPath { get; set; }
        /// <summary>
        /// 目标sheet名，将会复制到最后，然后改名
        /// </summary>
        private string destSheet { get; set; }

        private Excel.Workbook dataSourceWbk;
        private Excel.Workbook destWbk;

        public SingleSheetCopyProcessor(string dataSource, string srcSheet, string outputPath, string destSheet)
        {
            this.dataSource = dataSource;
            this.outputPath = outputPath;
            this.srcSheet = srcSheet;
            this.destSheet = destSheet;
        }

        public void process()
        {
            try
            {
                //读源文件
                dataSourceWbk = DataSources.ExcelApp.Workbooks.Open(this.dataSource);
                Excel.Worksheet dataSourceSht = dataSourceWbk.Sheets[srcSheet];

                //初始化目标文件
                destWbk = DataSources.ExcelApp.Workbooks.Open(this.outputPath);
                
                dataSourceSht.Copy(Type.Missing, destWbk.Sheets[destWbk.Sheets.Count]);
                destWbk.Sheets[destWbk.Sheets.Count].Name = destSheet;                


                //保存文件
                //destWbk.SaveAs(this.outputPath, Excel.XlFileFormat.xlExcel8, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                destWbk.Save();
           
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                destWbk.Close();
                //源文件不保存
                dataSourceWbk.Close(false);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace cn.jsfund.compliance.Utils
{
    public class SendMail
    {
        //method to send email to outlook
        public static void sendEMailThroughOUTLOOK(string subject, string body,
            string attchmentPath, string recipts)
        {
            try
            {
                // Create the Outlook application.
                Outlook.Application oApp = new Outlook.Application();
                // Create a new mail item.
                Outlook.MailItem oMsg = (Outlook.MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                // Set HTMLBody. 
                //add the body of the email
                oMsg.HTMLBody = body;
                //Add an attachment.
                String sDisplayName = "MyAttachment";
                int iPosition = (int)oMsg.HTMLBody.Length + 1;
                int iAttachType = (int)Outlook.OlAttachmentType.olByValue;
                //now attached the file
                if (!string.IsNullOrEmpty(attchmentPath))
                {
                    Outlook.Attachment oAttach = oMsg.Attachments.Add(attchmentPath, iAttachType, iPosition, sDisplayName);
                }
                //Subject line
                oMsg.Subject = subject;
                // Add a recipient.
                Outlook.Recipients oRecips = (Outlook.Recipients)oMsg.Recipients;
                // Change the recipient in the next line if necessary.
                foreach (string rec in recipts.Split(';'))
                {
                    Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(rec);
                    oRecip.Resolve();
                    oRecip = null;
                }

                // Send.
                oMsg.Send();
                // Clean up.

                oRecips = null;
                oMsg = null;
                oApp = null;
            }//end of try block
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }//end of catch
        }//end of Email Method
    }
}

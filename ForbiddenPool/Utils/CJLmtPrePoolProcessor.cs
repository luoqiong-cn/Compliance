﻿using System;
using System.Collections.Generic;

using System.Text;


using Excel = Microsoft.Office.Interop.Excel;
using cn.jsfund.compliance.Utils;
using System.Text.RegularExpressions;

namespace cn.jsfund.compliance.Utils
{
    /// <summary>
    /// 长江受托年金限投处理
    /// 数据源：\\hfm-ywshare\ZXData\CaiHui\长江养老禁投，限制基金池
    /// 王海：市场名称要调整一下，4改为6，文件里面的市场4是错的
    /// 把源数据先复制一份，再改数
    /// </summary>
    public class CJLmtPrePoolProcessor
    {
        private string dataSource { get; set; }
        private string srcSheet { get; set; }
        private string outputPath { get; set; }

        private Excel.Workbook dataSourceWbk;
        private Excel.Workbook configWbk;
        private Excel.Workbook destWbk;

        public CJLmtPrePoolProcessor(string dataSource, string srcSheet, string outputPath)
        {
            this.dataSource = dataSource;
            this.srcSheet = srcSheet;
            this.outputPath = outputPath;
        }

        public void process()
        {
            try
            {

                //读源文件
                dataSourceWbk = DataSources.ExcelApp.Workbooks.Open(this.dataSource);
                Excel.Worksheet dataSourceSht = dataSourceWbk.Sheets[srcSheet];
                Excel.Range dataSourceRng = dataSourceSht.UsedRange;

                //初始化目标文件
                destWbk = DataSources.ExcelApp.Workbooks.Add();
                Excel.Worksheet destSht = destWbk.Sheets[1];

                //开始处理
                //1. 复制源文件到目标文件
                dataSourceRng.Copy(destSht.Cells[1, 1]);

                //2. 对目标文件的备注列做关键字过滤
                Excel.Range destRng = destSht.UsedRange;
                int rowCount = destRng.Cells.Rows.Count;
                int i = 2;
                //筛选，正则表达式，如果filter是空则不过滤
                //这个地方不能用for循环，因为删掉行的话range就变了，应该在保留时移到下一行，删除时不移动
                while (destRng.Cells[i, 4].Value != null)
                {
                    if (destRng.Cells[i, 4].Value == "4")
                    {
                        destRng.Cells[i, 4].Value = "6";
                    }

                    i++;
                }

                //保存文件
                destWbk.SaveAs(this.outputPath, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                destWbk.Close();
                dataSourceWbk.Close(false);
            }
        }
    }
}

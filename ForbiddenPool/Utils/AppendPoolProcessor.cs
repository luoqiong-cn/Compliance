﻿using System;
using System.Collections.Generic;

using System.Text;


using Excel = Microsoft.Office.Interop.Excel;
using cn.jsfund.compliance.Utils;
using System.Text.RegularExpressions;

namespace cn.jsfund.compliance.Utils
{
    /// <summary>
    /// 这个processor用于将配置文件里的列表按池子分别追加到一个源文件，然后生成多个文件
    /// 平安、太平年金禁投池都是这种处理方法
    /// </summary>
    public class AppendPoolProcessor
    {
        private string dataSource { get; set; }
        private string filter { get; set; }
        private string appender { get; set; }
        private string outputPath { get; set; }
        private string configPath { get; set; }
        private string fbdFileNames { get; set; }

        private Excel.Workbook dataSourceWbk;
        private Excel.Workbook configWbk;
        private Excel.Workbook destWbk;

        public AppendPoolProcessor(string dataSource, string filter, string configPath, string appender, 
            string outputPath, string fbdFileNames)
        {
            this.dataSource = dataSource;
            this.filter = filter;
            this.configPath = configPath;
            this.appender = appender;
            this.outputPath = outputPath;
            this.fbdFileNames = fbdFileNames;
        }

        public void process()
        {
            processFbd();
            Console.WriteLine("禁投处理完成");
        }

        public void processFbd()
        {
            try
            {
                //读源文件
                dataSourceWbk = DataSources.ExcelApp.Workbooks.Open(this.dataSource);
                Excel.Worksheet dataSourceSht = dataSourceWbk.Sheets[1];
                Excel.Range dataSourceRng = dataSourceSht.UsedRange;

                //这两句仅仅是为了初始化一下，避免编译不过
                Excel.Worksheet configSht = dataSourceSht;
                Excel.Range configRng = configSht.UsedRange;

                //读禁投配置文件
                if (!string.IsNullOrEmpty(configPath))
                {
                    configWbk = DataSources.ExcelApp.Workbooks.Open(configPath);
                    configSht = configWbk.Sheets[this.appender];
                    configRng = configSht.UsedRange;
                }

                //2. 对目标文件的备注列做关键字过滤                
                int rowCount = dataSourceRng.Cells.Rows.Count;
                int i = 2;
                //筛选，正则表达式，如果filter是空则不过滤
                //这个地方不能用for循环，因为删掉行的话range就变了，应该在保留时移到下一行，删除时不移动
                if (!string.IsNullOrEmpty(filter))
                {
                    while (dataSourceRng.Cells[i, 5].Value != null)
                    {
                        if (!Regex.IsMatch(dataSourceRng.Cells[i, 5].Value, this.filter))
                        {
                            Excel.Range delRng = (Excel.Range)dataSourceRng.Rows[i, Type.Missing];
                            delRng.Delete(Excel.XlDeleteShiftDirection.xlShiftUp);
                        }
                        else
                            i++;
                    }
                }

                foreach (string fileName in this.fbdFileNames.Split(';'))
                {
                    destWbk = DataSources.ExcelApp.Workbooks.Add();
                    Excel.Worksheet destShtFbd = destWbk.Sheets[1];

                    dataSourceRng.Copy(destShtFbd.Cells[1, 1]);                    

                    configRng.AutoFilter(3, fileName);

                    rowCount = destShtFbd.UsedRange.Rows.Count;
                    configRng.Rows["2:100"].Copy(destShtFbd.Cells[rowCount + 1, 1]);

                    //1.1. 把最后一列删了，否则恒生无法导入
                    Excel.Range lastCol = (Excel.Range)destShtFbd.UsedRange.Columns[6];
                    lastCol.Delete();

                    destWbk.SaveAs(this.outputPath + DateTime.Now.ToString("yyyyMMdd") + "\\禁投池_" + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    destWbk.Close();
                    Console.WriteLine(fileName + "生成完成");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                //配置文件和数据源文件都不保存
                if (!String.IsNullOrEmpty(configPath))
                    configWbk.Close(false);
                dataSourceWbk.Close(false);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;
using cn.jsfund.compliance.Utils;

namespace cn.jsfund.compliance.Utils
{
    /// <summary>
    /// 这个processor用于把source里的第一个sheet按照sheets名多复制几份，
    /// 每一个sheet其实是恒生里的一个池的名称，恒生要求按这个格式
    /// </summary>
    public class SheetCopyProcessor
    {
        private string dataSource { get; set; }
        private string outputPath { get; set; }
        private string sheets { get; set; }

        private Excel.Workbook dataSourceWbk;
        private Excel.Workbook destWbk;

        public SheetCopyProcessor(string dataSource, string outputPath, string sheets)
        {
            this.dataSource = dataSource;
            this.outputPath = outputPath;
            this.sheets = sheets;
        }

        public void process()
        {
            try
            {
                //读源文件
                dataSourceWbk = DataSources.ExcelApp.Workbooks.Open(this.dataSource);
                Excel.Worksheet dataSourceSht = dataSourceWbk.Sheets[1];

                //初始化目标文件
                destWbk = DataSources.ExcelApp.Workbooks.Add();

                foreach (string sheetName in sheets.Split(';'))
                {
                    dataSourceSht.Copy(Type.Missing, destWbk.Sheets[destWbk.Sheets.Count]);
                    destWbk.Sheets[destWbk.Sheets.Count].Name = sheetName;

                    //1.1. 把最后一列删了，否则恒生无法导入
                    Excel.Range lastCol = (Excel.Range)destWbk.Sheets[sheetName].UsedRange.Columns[6];
                    lastCol.Delete();
                }

                //把前三个自动生成的sheet删掉
                destWbk.Sheets[1].Delete();
                destWbk.Sheets[1].Delete();
                destWbk.Sheets[1].Delete();

                //保存文件
                destWbk.SaveAs(this.outputPath, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
           
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                destWbk.Close();
                //源文件不保存
                dataSourceWbk.Close(false);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Collections;

using Excel = Microsoft.Office.Interop.Excel;
using cn.jsfund.compliance.Utils;

namespace cn.jsfund.compliance.Utils
{
    /// <summary>
    /// 验证恒生导入结果，将恒生导入的日志和原始文件比较
    /// </summary>
    public class ImportValidator
    {
        //比较是以left为基准
        private string leftFileName { get; set; }
        private string leftSheetName { get; set; }
        private string rightFileName { get; set; }
        private string rightSheetName { get; set; }

        Excel.Workbook leftWbk;
        Excel.Workbook rightWbk;

        public ImportValidator(string leftFileName, string leftSheetName, string rightFileName, string rightSheetName)
        {
            this.leftFileName = leftFileName;
            this.leftSheetName = leftSheetName;
            this.rightFileName = rightFileName;
            this.rightSheetName = rightSheetName;
        }

        public Dictionary<string, string> compare()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            try
            {
                //初始化左表
                leftWbk = DataSources.ExcelApp.Workbooks.Open(this.leftFileName);
                Excel.Worksheet leftSht = leftWbk.Sheets[this.leftSheetName];
                Excel.Range leftRng = leftSht.UsedRange;

                //初始化右表
                rightWbk = DataSources.ExcelApp.Workbooks.Open(this.rightFileName);
                Excel.Worksheet rightSht = rightWbk.Sheets[this.rightSheetName];
                Excel.Range rightRng = rightSht.UsedRange;
                //遍历一下右表，放到dictionary里，因为要用很多次，避免轮询
                Dictionary<string, string> rightDict = new Dictionary<string, string>();
                for (int i = 2; i <= rightRng.Cells.Rows.Count; i++)
                {
                    if (!rightDict.ContainsKey(rightRng.Cells[i, 1].Value) && rightRng.Cells[i, 3].Value != "已经存在")
                        rightDict.Add(rightRng.Cells[i, 1].Value, rightRng.Cells[i, 3].Value);                 
                }

                //遍历左表，在右表dict里找
                for (int i = 2; i <= leftRng.Cells.Rows.Count; i++)
                {
                    string key = string.Format("{0}", leftRng.Cells[i, 2].Value);
                    if (!rightDict.ContainsKey(key))
                    {
                        if (!result.ContainsKey(key))
                            result.Add(key, "导入结果中未找到此证券");
                    }
                    else if (rightDict[key] != "修改" 
                        && rightDict[key] != "已经存在" 
                        && rightDict[key] != "删除"
                        && rightDict[key] != "增加")
                    {
                        if (!result.ContainsKey(key))
                            result.Add(key, rightDict[key]);
                    }
                    //Console.WriteLine(i);
                }
                
            }
            catch (Exception ex)
            {
                result.Add(this.rightSheetName, ex.Message);
                //throw ex;
            }
            finally
            {
                leftWbk.Close(false);
                rightWbk.Close(false);
            }
            return result;   
        }
    }
}

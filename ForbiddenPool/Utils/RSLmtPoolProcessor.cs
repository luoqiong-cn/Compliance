﻿using System;
using System.Collections.Generic;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using cn.jsfund.compliance.Utils;
using System.Text.RegularExpressions;

namespace cn.jsfund.compliance.Utils
{
    /// <summary>
    /// 人寿限投有点特殊，这个类专门处理
    /// </summary>
    public class RSLmtPoolProcessor
    {
        private string dataSource { get; set; }
        private string filter { get; set; }
        private string appender { get; set; }
        private string outputPath { get; set; }
        private string configPath { get; set; }

        private Excel.Workbook dataSourceWbk;
        private Excel.Workbook configWbk;
        private Excel.Workbook destWbk;

        public RSLmtPoolProcessor(string dataSource, string filter, string configPath, string appender, string outputPath)
        {
            this.dataSource = dataSource;
            this.filter = filter;
            this.configPath = configPath;
            this.appender = appender;
            this.outputPath = outputPath;
        }

        public void process()
        {
            try
            {

                //读源文件
                dataSourceWbk = DataSources.ExcelApp.Workbooks.Open(this.dataSource);
                Excel.Worksheet dataSourceSht = dataSourceWbk.Sheets[1];
                Excel.Range dataSourceRng = dataSourceSht.UsedRange;

                //这两句仅仅是为了初始化一下，避免编译不过
                Excel.Worksheet configSht = dataSourceSht;
                Excel.Range configRng = configSht.UsedRange;

                //读配置文件
                if (!string.IsNullOrEmpty(configPath))
                {
                    configWbk = DataSources.ExcelApp.Workbooks.Open(configPath);
                    configSht = configWbk.Sheets[this.appender];
                    configRng = configSht.UsedRange;
                    //把标题行删了
                    Excel.Range headerRng = (Excel.Range)configRng.Rows[1, Type.Missing];
                    headerRng.Delete(Excel.XlDeleteShiftDirection.xlShiftUp);
                }

                //初始化目标文件
                destWbk = DataSources.ExcelApp.Workbooks.Add();
                Excel.Worksheet destSht = destWbk.Sheets[1];

                //开始处理
                //1. 复制源文件到目标文件
                dataSourceRng.Copy(destSht.Cells[1, 1]);

                //2. 对目标文件的备注列做关键字过滤
                Excel.Range destRng = destSht.UsedRange;
                int rowCount = destRng.Cells.Rows.Count;
                int i = 2;
                //筛选，正则表达式，如果filter是空则不过滤
                //这个地方不能用for循环，因为删掉行的话range就变了，应该在保留时移到下一行，删除时不移动
                if (!string.IsNullOrEmpty(filter))
                {
                    while (destRng.Cells[i, 5].Value != null)
                    {
                        if (Regex.IsMatch(destRng.Cells[i, 5].Value, this.filter) || !Regex.IsMatch(destRng.Cells[i, 5].Value, DataSources.RS_LMT_FILTER))
                        {
                            Excel.Range delRng = (Excel.Range)destRng.Rows[i, Type.Missing];
                            delRng.Delete(Excel.XlDeleteShiftDirection.xlShiftUp);
                        }
                        else
                            i++;
                    }
                }

                //3. 追加名单，把配置文件里相应sheet页里的内容复制到目标文件
                //重算一下行数
                if (!string.IsNullOrEmpty(configPath))
                {
                    rowCount = destSht.UsedRange.Rows.Count;
                    configRng.Copy(destSht.Cells[rowCount + 1, 1]);
                }

                //保存文件
                destWbk.SaveAs(this.outputPath, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                destWbk.Close();
                //配置文件和数据源文件都不保存
                if(!String.IsNullOrEmpty(configPath))
                    configWbk.Close(false); 
                dataSourceWbk.Close(false);
            }
        }
    }
}

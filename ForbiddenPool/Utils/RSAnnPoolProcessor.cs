﻿using System;
using System.Collections.Generic;

using System.Text;


using Excel = Microsoft.Office.Interop.Excel;
using cn.jsfund.compliance.Utils;
using System.Text.RegularExpressions;

namespace cn.jsfund.compliance.Utils
{
    /// <summary>
    /// 人寿受托年金处理
    /// 数据源：\\hfm-ywshare.js.local\ZXData\CaiHui\人寿养老禁投\20170330\禁投池_标准版本20170330.XLS
    /// 数据源里同时包括禁限投，根据备注列区分
    /// 配置文件：配置文件里包括所有人寿受托年金的列表，以及每个年金都需要追加哪些股票
    /// 处理：限投：从数据源把限投股票弄出来，然后按每个年金一个sheet页复制很多份保存到一个限投文件
    /// 禁投：从数据源把禁投弄出来，每个年金追加各自的股票，各保存到一个禁投文件
    /// </summary>
    public class RSAnnPoolProcessor
    {
        private string dataSource { get; set; }
        private string filter { get; set; }
        private string appender { get; set; }
        private string outputPath { get; set; }
        private string configPath { get; set; }
        private string lmtSheetNames { get; set; }
        private string fbdFileNames { get; set; }

        private Excel.Workbook dataSourceWbk;
        private Excel.Workbook configWbk;
        private Excel.Workbook destWbk;

        public RSAnnPoolProcessor(string dataSource, string filter, string configPath, string appender, 
            string outputPath, string lmtSheetNames, string fbdFileNames)
        {
            this.dataSource = dataSource;
            this.filter = filter;
            this.configPath = configPath;
            this.appender = appender;
            this.outputPath = outputPath;
            this.lmtSheetNames = lmtSheetNames;
            this.fbdFileNames = fbdFileNames;
        }

        public void process()
        {
            if (!string.IsNullOrEmpty(this.lmtSheetNames))
            {
                processLmt();
                Console.WriteLine("限投处理完成");
            }
            if (!string.IsNullOrEmpty(this.fbdFileNames))
            {
                processFbd();
                Console.WriteLine("禁投处理完成");
            }
        }

        public void processLmt()
        {
            try
            {

                //读源文件
                dataSourceWbk = DataSources.ExcelApp.Workbooks.Open(this.dataSource);
                Excel.Worksheet dataSourceSht = dataSourceWbk.Sheets[1];
                Excel.Range dataSourceRng = dataSourceSht.UsedRange;

                //初始化目标文件
                destWbk = DataSources.ExcelApp.Workbooks.Add();
                Excel.Worksheet destShtLmt = destWbk.Sheets[1];

                //1. 复制限投数据到目标文件
                dataSourceRng.AutoFilter(3, "限投");
                dataSourceRng.Copy(destShtLmt.Cells[1, 1]);
                //1.1. 把最后一列删了，否则恒生无法导入
                Excel.Range lastCol = (Excel.Range)destShtLmt.UsedRange.Columns[6];
                lastCol.Delete();

                //按照名字列表复制N份
                foreach (string sheetName in this.lmtSheetNames.Split(';'))
                {
                    destShtLmt.Copy(Type.Missing, destWbk.Sheets[destWbk.Sheets.Count]);
                    destWbk.Sheets[destWbk.Sheets.Count].Name = sheetName;
                }

                DataSources.ExcelApp.DisplayAlerts = false;
                //把前三个自动生成的sheet删掉
                destWbk.Sheets[1].Delete();
                destWbk.Sheets[1].Delete();
                destWbk.Sheets[1].Delete();
                DataSources.ExcelApp.DisplayAlerts = true;

                //保存文件
                destWbk.SaveAs(this.outputPath + DateTime.Now.ToString("yyyyMMdd") + "\\人寿受托年金限投池_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                destWbk.Close();
                dataSourceWbk.Close(false);
            }
        }

        public void processFbd()
        {
            try
            {
                //读源文件
                dataSourceWbk = DataSources.ExcelApp.Workbooks.Open(this.dataSource);
                Excel.Worksheet dataSourceSht = dataSourceWbk.Sheets[1];
                Excel.Range dataSourceRng = dataSourceSht.UsedRange;

                //这两句仅仅是为了初始化一下，避免编译不过
                Excel.Worksheet configSht = dataSourceSht;
                Excel.Range configRng = configSht.UsedRange;

                //读禁投配置文件
                if (!string.IsNullOrEmpty(configPath))
                {
                    configWbk = DataSources.ExcelApp.Workbooks.Open(configPath);
                    configSht = configWbk.Sheets[this.appender];
                    configRng = configSht.UsedRange;
                }

                //2. 对目标文件的备注列做关键字过滤                
                int rowCount = dataSourceRng.Cells.Rows.Count;
                int i = 2;
                //筛选，正则表达式，如果filter是空则不过滤
                //这个地方不能用for循环，因为删掉行的话range就变了，应该在保留时移到下一行，删除时不移动
                if (!string.IsNullOrEmpty(filter))
                {
                    while (dataSourceRng.Cells[i, 5].Value != null)
                    {
                        if (!Regex.IsMatch(dataSourceRng.Cells[i, 5].Value, this.filter))
                        {
                            Excel.Range delRng = (Excel.Range)dataSourceRng.Rows[i, Type.Missing];
                            delRng.Delete(Excel.XlDeleteShiftDirection.xlShiftUp);
                        }
                        else
                            i++;
                    }
                }

                //dataSourceRng.AutoFilter(3, "禁投");

                foreach (string fileName in this.fbdFileNames.Split(';'))
                {
                    destWbk = DataSources.ExcelApp.Workbooks.Add();
                    Excel.Worksheet destShtFbd = destWbk.Sheets[1];

                    dataSourceRng.AutoFilter(3, "禁投");
                    dataSourceRng.Copy(destShtFbd.Cells[1, 1]);

                    rowCount = destShtFbd.UsedRange.Rows.Count;
                    dataSourceRng.AutoFilter(3, "禁持");
                    dataSourceRng.Copy(destShtFbd.Cells[rowCount + 1, 1]);
                    Excel.Range headerRow = (Excel.Range)destShtFbd.UsedRange.Rows[rowCount + 1, Type.Missing];
                    headerRow.Delete();

                    configRng.AutoFilter(3, fileName);

                    rowCount = destShtFbd.UsedRange.Rows.Count;
                    configRng.Rows["2:100"].Copy(destShtFbd.Cells[rowCount + 1, 1]);

                    //1.1. 把最后一列删了，否则恒生无法导入
                    Excel.Range lastCol = (Excel.Range)destShtFbd.UsedRange.Columns[6];
                    lastCol.Delete();

                    destWbk.SaveAs(this.outputPath + DateTime.Now.ToString("yyyyMMdd") + "\\人寿受托年金禁投池_" + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    destWbk.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                //配置文件和数据源文件都不保存
                if (!String.IsNullOrEmpty(configPath))
                    configWbk.Close(false);
                dataSourceWbk.Close(false);
            }
        }
    }
}

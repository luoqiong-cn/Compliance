﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using cn.jsfund.compliance.Utils;
using System.IO;
using System.Configuration;
using System.IO;

namespace ImportValidatorApp
{
    class Program
    {
        static void Main(string[] args)
        {
            StringBuilder sb = new StringBuilder();
            List<Tuple<string, string, string, string, string>> config = loadConfig(ConfigurationManager.AppSettings["IMPORT_MAPPING"]);
            sb.AppendLine("开始校验");
            sb.AppendLine("<br/>");
            //ImportValidator iv = new ImportValidator(@"d:\pool\东风汽车禁投股票池20170322.xls", "Sheet1",
            //    @"d:\pool\导入报告_20170322084652_.xls", "东风汽车禁止股票池");

            //Dictionary<string, string> result = iv.compare();

            //foreach (string key in result.Keys)
            //{
            //    sb.AppendLine(key + "----------" + result[key]);
            //}
            //sb.AppendLine("校验完成");
            //sb.AppendLine("903");
            //iv = new ImportValidator(@"d:\pool\基本养老903组合限投股票池20170322.xls", "Sheet1",
            //    @"d:\pool\导入报告_20170322084652_.xls", "基本养老保险903禁止池");

            //result = iv.compare();

            //foreach (string key in result.Keys)
            //{
            //    sb.AppendLine(key + "----------" + result[key]);
            //}
            string d = DateTime.Now.ToString("yyyyMMdd");
            StreamWriter sw = new System.IO.StreamWriter(
                DataSources.OUTPUT_PATH.Replace("YYYYMMDD", d)
                ,false, Encoding.UTF8);

            //sw.Write(new byte[]{0xEF, 0xBB, 0xBF});
            
            sw.WriteLine("恒生组合代码,禁限投池名称,证券代码,状态说明");
            sw.Flush();


            foreach (var item in config)
            {
                sb.AppendLine(item.Item4 + "开始验证");
                Console.WriteLine(item.Item4 + "开始验证");
                sb.AppendLine("<br/>");
                ImportValidator iv = new ImportValidator(item.Item1.Replace("YYYYMMDD", d),
                    item.Item2.Replace("YYYYMMDD", d),
                    item.Item3.Replace("YYYYMMDD", d),
                    item.Item4);
                Dictionary<string, string> result = iv.compare();
                foreach (string key in result.Keys)
                {
                    sb.AppendLine(key + "----------" + result[key]);
                    sb.AppendLine("<br/>");

                    sw.WriteLine(string.Format(@"{0},{1},=""{2}"",=""{3}""", item.Item5, item.Item4, key, result[key]));
                }
                sb.AppendLine(item.Item4 + "验证结束");
                sb.AppendLine("<br/>");
                sw.Flush();
            }
            sb.AppendLine("校验完成");
            Console.WriteLine("校验完成");
            sb.AppendLine("<br/>");
            sw.Close();

            //发邮件
            //SendMail.sendEMailThroughOUTLOOK("禁限投池导入验证"+d, sb.ToString(), "", 
            //    "luocw@jsfund.cn;yangxy@jsfund.cn;houkun@jsfund.cn;luoqiong@jsfund.cn");
            SendMail.sendEMailThroughOUTLOOK("禁限投池导入验证" + d, "",
                DataSources.OUTPUT_PATH.Replace("YYYYMMDD", d),
                "luoqiong@jsfund.cn");

        }

        /// <summary>
        /// 从指定路径读取配置文件，配置文件每一行放入一个Tuple，分别表示要验证的左表路径和右表sheet页名称
        /// （左表的sheet页名称是知道的Sheet1，右表的文件名是知道的，）
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static List<Tuple<string, string, string, string,string >> loadConfig(string path)
        {
            List<Tuple<string, string, string, string, string>> config = new List<Tuple<string, string, string, string, string>>();
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    string line;
                    while (!string.IsNullOrEmpty(line = sr.ReadLine()) )   //用//表示注释行，要不太多分不清楚
                    {
                        if (!line.StartsWith("//"))
                        {
                            string[] arr = line.Split(';');
                            config.Add(new Tuple<string, string, string, string, string>(arr[0], arr[1], arr[2], arr[3], arr[4]));
                        }
                        else
                            continue;
                    }
                }
            }
            catch (Exception ex)
            {                
                throw ex;
            }        
            
            return config;
        }
    }
}

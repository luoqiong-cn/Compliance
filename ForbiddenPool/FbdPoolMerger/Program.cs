﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;
using cn.jsfund.compliance.Utils;
using System.Configuration;
using System.IO;

namespace FbdPoolMerger
{
    class Program
    {
        static void Main(string[] args)
        {
            string outputPath;
            if (args.Length > 0)
                outputPath = args[0] + "\\";
            else
                outputPath = DataSources.OUTPUT_PATH;

            string strDate = System.DateTime.Now.ToString("yyyyMMdd");

            //准备输出目录
            if (Directory.Exists(outputPath + strDate))
                Directory.Delete(outputPath + strDate, true);
            Directory.CreateDirectory(outputPath + strDate);

            List<Tuple<string, string, string, string>> config = loadConfig(ConfigurationManager.AppSettings["MAPPING"]);
            foreach (var item in config)
            {
                PoolMerger pm = new PoolMerger(item.Item1.Replace("YYYYMMDD", strDate),
                    item.Item2.Replace("YYYYMMDD", strDate),
                    item.Item3.Replace("YYYYMMDD", strDate),
                    item.Item4.Replace("YYYYMMDD", strDate));
                pm.process();
            }
            Console.WriteLine("禁投合并完成");
                        
            //最后一步，把temp文件都复制重命名一下，给恒生这个文件，避免恒生读到中间文件
            foreach (var f in Directory.GetFiles(outputPath + strDate))
            {
                //先把每个文件里三个自动生成的Sheet页删掉，要不恒生没法导入
                Excel.Workbook wbk = DataSources.ExcelApp.Workbooks.Open(f);
                //把前三个自动生成的sheet删掉
                wbk.Sheets[1].Delete();
                wbk.Sheets[1].Delete();
                wbk.Sheets[1].Delete();
                wbk.Save();

                File.Copy(f, f.Replace("_temp", ""));
            }

            Console.WriteLine("文件重命名完成");

            //把限投拷贝过来
            File.Copy(string.Format(@"\\hfm-ywshare.js.local\ZXData\平安年金禁限投股票池\{0}\平安年金限投{0}.xlsx", strDate), 
                outputPath + strDate + String.Format(@"\平安年金限投{0}.xlsx", strDate));
            File.Copy(string.Format(@"\\hfm-ywshare.js.local\ZXData\每日禁限投池\{0}\平安委托帐户股票限投O32_{0}.xlsx", strDate),
                outputPath + strDate + String.Format(@"\平安委托帐户股票限投O32_{0}.xlsx", strDate));
            File.Copy(string.Format(@"\\hfm-ywshare.js.local\ZXData\人寿托管年金禁限投股票池\池1\{0}\人寿受托年金限投池0_{0}.xlsx", strDate),
                outputPath + strDate + String.Format(@"\人寿受托年金限投池0_{0}.xlsx", strDate));
            File.Copy(string.Format(@"\\hfm-ywshare.js.local\ZXData\人寿托管年金禁限投股票池\池2\{0}\人寿受托年金限投池_{0}.xlsx", strDate),
                outputPath + strDate + String.Format(@"\人寿受托年金限投池_{0}.xlsx", strDate));
            File.Copy(string.Format(@"\\hfm-ywshare.js.local\ZXData\太平年金禁限投股票池\{0}\太平年金限投{0}.xlsx", strDate),
                outputPath + strDate + String.Format(@"\太平年金限投{0}.xlsx", strDate));
        }

        /// <summary>
        /// 从指定路径读取配置文件，配置文件每一行放入一个Tuple，分别表示源和目标
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static List<Tuple<string, string, string, string>> loadConfig(string path)
        {
            List<Tuple<string, string, string, string>> config = new List<Tuple<string, string, string, string>>();
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    string line;
                    while (!string.IsNullOrEmpty(line = sr.ReadLine()))   //用//表示注释行，要不太多分不清楚
                    {
                        if (!line.StartsWith("//"))
                        {
                            string[] arr = line.Split(';');
                            config.Add(new Tuple<string, string, string, string>(arr[0], arr[1], arr[2], arr[3]));
                        }
                        else
                            continue;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return config;
        }
    }

    /// <summary>
    /// 这个类用于将之前的分文件的禁投池合并成一个文件多个sheet的形式，便于恒生使用
    /// </summary>
    class PoolMerger
    {
        private string dataSource { get; set; }
        private string srcSheet { get; set; }
        /// <summary>
        /// 没有则创建
        /// </summary>
        private string outputPath { get; set; }
        /// <summary>
        /// 目标sheet名，将会复制到最后，然后改名
        /// </summary>
        private string destSheet { get; set; }

        public PoolMerger(string dataSource, string srcSheet, string outputPath, string destSheet)
        {
            this.dataSource = dataSource;
            this.outputPath = outputPath;
            this.srcSheet = srcSheet;
            this.destSheet = destSheet;
        }

        /// <summary>
        /// 主要干的一件事就是目标文件如果没有就创建出来，然后调用SingleSheetCopyProcessor处理
        /// </summary>
        public void process()
        {
            if (!File.Exists(outputPath))
            {
                Excel.Workbook destWbk = DataSources.ExcelApp.Workbooks.Add();
                destWbk.SaveAs(this.outputPath, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlExclusive,Excel.XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            }

            SingleSheetCopyProcessor sscp = new SingleSheetCopyProcessor(dataSource, srcSheet, outputPath, destSheet);
            sscp.process();
            Console.WriteLine(destSheet + "复制完成");
        }
    }
}

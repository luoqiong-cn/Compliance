﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using cn.jsfund.compliance.Utils;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;


namespace cn.jsfund.compliance
{
    class TempAnnouncement
    {
        static void Main(string[] args)
        {
            string annString;   //从网站上读的原始字符串
            List<string> annlist = new List<string>(); //公告标题列表
            StringBuilder htmlbody = new StringBuilder();
            string htmlheader;
            StringBuilder recps = new StringBuilder();
            StringBuilder recpsAttach = new StringBuilder(); 

            string startDate;   //从配置文件里读
            using (StreamReader sr = new StreamReader(Directory.GetCurrentDirectory() + "./config_attach.txt"))
            {
                startDate = sr.ReadToEnd();
            }
            startDate = DateTime.Parse(startDate).AddDays(1).ToString("yyyy-MM-dd");

            string endDate = System.DateTime.Now.ToString("yyyy-MM-dd");    //写死当天
            using (StreamWriter sw = new StreamWriter(Directory.GetCurrentDirectory() + "./config_attach.txt", false))
            {
                sw.WriteLine(endDate);
                sw.Flush();
            } 
                        
            

            using (WebClient client = new WebClient())
            {
                client.Encoding = System.Text.Encoding.UTF8;
                
                //163发售公告
                //164基金合同
                //165招募说明书
                //167托管协议
                //157临时公告
                //158其他公告
                annString = client.DownloadString(
                    string.Format("http://www.jsfund.cn/izenportal/service/portal/findActionInfo?catalogid=163,164,165,167,157,158&startDate={0}&endDate={1}",
                        startDate, endDate)
                    );
            }

            JObject o = JObject.Parse(annString);
            JArray anns = (JArray)o["data"]["result"];

            if (anns != null)
            {
                foreach (var ann in anns.Children())
                {
                    annlist.Add((string)ann["title"]);
                }
            }

            htmlheader = string.Equals(startDate, endDate) ? "公司今日发布公告，敬请查阅" : string.Format("公司{0}日及今日发布公告，敬请查阅", DateTime.Parse(startDate).Day);
            htmlbody.Append("各位领导、同事：<br/>    早上好！");
            htmlbody.Append(htmlheader);
            htmlbody.Append("：<br/><br/><font color='red'>");
            for (int i = 0; i < annlist.Count; i++)
            {
                htmlbody.Append(string.Format("{0}. {1}<br/>", i + 1, annlist[i]));
            }

            htmlbody.Append("</font><br/>");
            htmlbody.Append("上述公告原文请见公司主页嘉实公告栏目：<a href='http://www.jsfund.cn/Funds/Notices/index.shtml'>http://www.jsfund.cn/Funds/Notices/index.shtml</a>");

            /*
            recps.Append("HFMHLWJRZX@jsfund.cn;"); //HFM互联网金融中心
            recps.Append("HFMKHFW@jsfund.cn;"); //HFM客户服务部
            recps.Append("HFMQDFZ@jsfund.cn;"); //HFM渠道发展部
            recps.Append("HFMIAADMIN@jsfund.cn;"); //HFM投资IA管理员组
            recps.Append("HFMJJYY@jsfund.cn;"); //HFM基金运营
            recps.Append("HFMZSTZB@jsfund.cn;"); //HFM指数投资部
            recps.Append("HFMFL&NK@jsfund.cn;"); //HFM法务和内控
            recps.Append("HFMYWTZ@jsfund.cn;"); //HFM产品管理部
            recps.Append("gongling@jsfund.cn;"); //龚凌
            recps.Append("EvaChung@hk.jsfund.cn;"); //Chung Eva
            recps.Append("chenshuo@jsfund.cn;"); //陈烁
            recps.Append("yugq@jsfund.cn;"); //于桂琦
            recps.Append("yangcheng@jsfund.cn;"); //杨成
            recps.Append("weigr@jsfund.cn;"); //魏国荣
            recps.Append("HFMZHKZB@jsfund.cn;"); //HFM组合控制部
            recps.Append("yangxy@jsfund.cn;"); //杨欣怡
            recps.Append("huyq@jsfund.cn;"); //胡勇钦
            
            recps.Append("luoqiong@jsfund.cn");

            SendMail.sendEMailThroughOUTLOOK(htmlheader, htmlbody.ToString(), null, recps.ToString());
            */
            /*
            recpsAttach.Append("chentl@jsfund.cn;");
            recpsAttach.Append("fuyl@jsfund.cn;");
            recpsAttach.Append("cfglsx01@jsfund.cn;");
            recpsAttach.Append("zhaojz@csrc.gov.cn;");
            recpsAttach.Append("huyq@jsfund.cn;");
            recpsAttach.Append("yangxy@jsfund.cn;");
            */
            
            recpsAttach.Append("luoqiong@jsfund.cn");
            SendMail.sendEMailThroughOUTLOOK(htmlheader, htmlbody.ToString(), getAttachments(startDate, endDate), recpsAttach.ToString());
            
        }

        static List<string> getAttachments(string startDate, string endDate)
        {
            List<string> ret = new List<string>();
            DateTime dt = DateTime.Parse(startDate);
            string path;
            while (dt <= DateTime.Parse(endDate))
            {
                path = string.Format(@"\\hfm-pubshare\HFM各部门共享\合规部\2018年公告发布\{0}.{1}", dt.Month, dt.Day);

                if (Directory.Exists(path))
                {
                    foreach (var filepath in Directory.GetFiles(path, "*.doc"))
                    {
                        ret.Add(filepath);
                    }
                }

                dt = dt.AddDays(1);
            }

            return ret;
        }
    }
}

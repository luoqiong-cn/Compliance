﻿using System;
using System.Collections.Generic;
using System.Text;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.IO;

namespace cn.jsfund.compliance.Utils
{
    public class SendMail
    {
        //method to send email to outlook
        public static void sendEMailThroughOUTLOOK(string subject, string body,
            List<string> attchments, string recipts)
        {
            try
            {
                // Create the Outlook application.
                Outlook.Application oApp = new Outlook.Application();
                // Create a new mail item.
                Outlook.MailItem oMsg = (Outlook.MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                // Set HTMLBody. 
                //add the body of the email
                oMsg.HTMLBody = body;
                //Add an attachment.
                String sDisplayName = "MyAttachment";
                int iPosition = 0;
                int iAttachType = (int)Outlook.OlAttachmentType.olByValue;
                //now attached the file
                if (attchments != null && attchments.Count > 0)
                {
                    foreach (var item in attchments)
                    {
                        iPosition = (int)oMsg.HTMLBody.Length + 1;
                        Outlook.Attachment oAttach = oMsg.Attachments.Add(item, iAttachType, iPosition, sDisplayName);
                    }
                    
                }
                //Subject line
                oMsg.Subject = subject;
                // Add a recipient.
                Outlook.Recipients oRecips = (Outlook.Recipients)oMsg.Recipients;
                // Change the recipient in the next line if necessary.
                foreach (string rec in recipts.Split(';'))
                {
                    Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(rec);
                    oRecip.Resolve();
                    oRecip = null;
                }

                // Send.
                Outlook.Account account = GetAccountForEmailAddress(oApp, "jcjh@jsfund.cn");
                oMsg.SendUsingAccount = account;
                oMsg.Send();
                // Clean up.

                oRecips = null;
                oMsg = null;
                oApp = null;
            }//end of try block
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }//end of catch
        }//end of Email Method

        public static Outlook.Account GetAccountForEmailAddress(Outlook.Application application, string smtpAddress)
        {

            // Loop over the Accounts collection of the current Outlook session. 
            Outlook.Accounts accounts = application.Session.Accounts;
            foreach (Outlook.Account account in accounts)
            {
                // When the e-mail address matches, return the account. 
                if (account.SmtpAddress == smtpAddress)
                {
                    return account;
                }
            }
            throw new System.Exception(string.Format("No Account with SmtpAddress: {0} exists!", smtpAddress));
        } 
    }
}

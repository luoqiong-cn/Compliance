Set-ExecutionPolicy -ExecutionPolicy UNRESTRICTED
$d = [DateTime]::Now.ToString("yyyyMMdd")
$oldName = [String]::Format("\\hfm-ywshare\ZXData\人寿托管年金禁限投股票池\池1\{0}\人寿受托年金限投池_{0}.xls", $d)
$newName = [String]::Format("\\hfm-ywshare\ZXData\人寿托管年金禁限投股票池\池1\{0}\人寿受托年金限投池1_{0}.xls", $d)
Rename-Item -Path $oldName -NewName $newName
# -*- coding: utf-8 -*-

import requests
import sys
import json
import codecs

# 取参数
#start_date = raw_input("输入开始日期(yyyy-mm-dd):")
#end_date = raw_input("输入结束日期(yyyy-mm-dd):")

start_date = '2018-01-31'
end_date = '2018-03-05'

print(start_date)
print(end_date)

annoucements = json.loads(
        requests.get('http://www.jsfund.cn/izenportal/service/portal/findActionInfo?catalogid=157&startDate={:s}&endDate={:s}'.format(start_date, end_date)).text)
#print(annoucements.text)

#所有基金及代码
fund_map = {}
funds = json.loads(
        requests.get('http://www.jsfund.cn/izenportal/service/left/getFundLeft').text)
for fund_by_type in funds['data']['result']:
    for fund in fund_by_type['array']:
        fund_map[fund['fundcode']] = fund['fundshortname']

out = codecs.open('./data_by_fund_{}-{}.csv'.format(start_date, end_date), mode='w', encoding='utf-8')
out.write(u'\ufeff')
out1 = codecs.open('./data_by_announcement_{}-{}.csv'.format(start_date, end_date), mode='w', encoding='utf-8')
out1.write(u'\ufeff')

#生成表头
out.write(u'公告ID|公告标题|公告日期|基金代码|基金名称')
out.write('\n')
out1.write(u'公告ID|公告标题|公告日期|基金代码|基金名称')
out1.write('\n')

for ann in annoucements['data']['result']:
    rel_funds = ann.get('prop2')
    if rel_funds == None:
        str = u'{:d}|{:s}|{:s}|{:s}|{:s}\n'.format(ann['id'], ann['title'], ann['publishdate'], '', '')
        print(str)
        out.write(str)
        out1.write(str)
    else:
        fund_names = ''
        for fund_no in rel_funds.split(','):
            str = u'{:d}|{:s}|{:s}|{:s}|{:s}\n'.format(ann['id'], ann['title'], ann['publishdate'], fund_no+'.OF', fund_map.get(fund_no,''))
            fund_names = fund_names  + ',' + fund_map.get(fund_no,'')
            print(str)
            out.write(str)
        rel_funds = rel_funds.replace(',','.OF,') + '.OF'
        str = u'{:d}|{:s}|{:s}|{:s}|{:s}\n'.format(ann['id'], ann['title'], ann['publishdate'], rel_funds, fund_names[1:])
        out1.write(str)


out.flush()
out.close()
out1.flush()
out1.close()
